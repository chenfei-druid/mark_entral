/**
 * @brief The rtc abstract layer
 *
 * @file rtc.c
 * @date 2018-06-22
 * @author Dengjian
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */


/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "timers.h"
#include "nrfx_log.h"
#include "hal_rtc.h"

/* Defines -------------------------------------------------------------------*/

/* Typedefs ------------------------------------------------------------------*/
typedef struct rtc_backup {
  uint32_t time;
  uint32_t check;
} rtc_backup_t;

/* Private variables ---------------------------------------------------------*/
static rtc_backup_t* _backup = (rtc_backup_t*) 0x2000fff8;
static TimerHandle_t _timer = NULL;
static uint32_t _uptime = 0; // Up time in seconds
static uint32_t _base = 0; // Base time in seconds

/* Private functions ---------------------------------------------------------*/
static void timer_callback(TimerHandle_t xTimer) {
  // Update time
  _uptime++;
  // Backup in nonvolatile ram
  _backup->time = _uptime + _base;
  _backup->check = ~_backup->time;
}

/* Global functions ----------------------------------------------------------*/

/**
 * @brief Init rtc
 *
 * @return HAL_ERR_SUCCESS if success
 */
hal_err_t hal_rtc_init(void) {
  static StaticTimer_t timer_memory = { 0 };

  // Recover from nonvolatile ram
  if(_backup->time == ~_backup->check) {
    _base = _backup->time;
  }

  _timer = xTimerCreateStatic("rtc_timer", pdMS_TO_TICKS(1000), pdTRUE, NULL, timer_callback, &timer_memory);
  if(!_timer) {
    return HAL_ERR_MEMORY;
  }

  if(xTimerStart(_timer, 0) != pdTRUE) {
    return HAL_ERR_BUSY;
  }

  return HAL_ERR_OK;
}

/**
 * @brief Set time
 *
 * @param time
 *
 * @return HAL_ERR_OK if success
 */
hal_err_t hal_rtc_set_time(uint32_t time) {
  if(!_timer || xTimerIsTimerActive(_timer) != pdTRUE) {
    return HAL_ERR_NOT_INITIALIZED;
  }

  _base = time - _uptime;
  return HAL_ERR_OK;
}

/**
 * @brief Get base
 *
 * @return base time in seconds
 */
uint32_t hal_rtc_get_boot_time(void) {
  if(!_timer || xTimerIsTimerActive(_timer) != pdTRUE) {
    return 0;
  }

  return _base;
}

/**
 * @brief Get time
 *
 * @return time in seconds
 */
uint32_t hal_rtc_get_time(void) {
  if(!_timer || xTimerIsTimerActive(_timer) != pdTRUE) {
    return 0;
  }

  return _uptime + _base;
}

/**
 * @brief Get uptime
 *
 * @return uptime in seconds
 */
uint32_t hal_rtc_get_uptime(void) {
  if(!_timer || xTimerIsTimerActive(_timer) != pdTRUE) {
    return 0;
  }

  return _uptime;
}
