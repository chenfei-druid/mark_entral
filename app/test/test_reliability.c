/**
 * @brief 
 * 
 * @file test_reliability.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-20
 */
#include "stdint.h" 
#include "ble_trans.h"
#include "test_reliability.h"
#include "user_app_log.h"
#include "ble_druid_svc.h"
#include "nrfx_gpiote.h"
#include "hal_pin.h"

#define LED_TRANS          10

static TaskHandle_t  reli_handle = NULL;
static  QueueHandle_t       queue_work_mode=NULL;
static bool     run_status = 0;
//static char const m_target_periph_name[] = "ble_slave";
//static uint8_t slave_mac[] = {0xfd,0xe7,0xd2,0xfc,0xdf,0xd6};

bool get_sample_status(void)
{
    return run_status;
}
/**@brief Function for read g-sensor data, and process to send over ble.
 *
 * @details This function will be called by main loop, if g-sensor fifo interrupt, the flag will be set ,that will be check at main loop.
 *        
 */

void reliability_test_task(void *arg)
{            
    DBG_LOG(".......reliability_test_task startup .......");
    static uint8_t cmd_buf[BLE_RX_BUF_SIZE] = {0};
    static uint32_t len;
    hal_pin_set_mode(LED_TRANS,HAL_PIN_MODE_OUT);
    hal_pin_write(LED_TRANS,0);
    while(1)
    {
        len = ble_c_receive(cmd_buf,sizeof(cmd_buf),1000);
        if(len > 0)
        {
            //when received data on ble , upload all data to PC at once
            DBG_LOG("ble received len = %d",len);
            ble_s_pkg_send(cmd_buf,len);
            memset(cmd_buf,0,sizeof(cmd_buf));
            len = 0;         
        }
        hal_pin_write(LED_TRANS,1);
        vTaskDelay(10);
        hal_pin_write(LED_TRANS,0);
    }
}
TaskHandle_t create_sample_task(void)
{
    uint32_t ret = xTaskCreate( reliability_test_task, "reli", 512, NULL, 1, &reli_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    return reli_handle;
}
TaskHandle_t reli_task_delete(void)
{
    vTaskDelete(reli_handle);
    return reli_handle;
}
void suspend_reli_task(void)
{
    run_status = 0;
    vTaskSuspend(reli_handle);
}
void resume_reli_task(void)
{
    run_status = 1;
    vTaskResume(reli_handle);
}

