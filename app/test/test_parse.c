/**
 * @brief 
 * 
 * @file test_parse.c
 * @date 2018-08-03
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include "test_parse.h"
#include "crc16.h"
#include "user_app_log.h"
#include "user_data_pkg.h"
#include "test_parse.h"
#include "define.pb.h"
#include "centr_conn.h"
#include "define.pb.h"
#include "scan.h"

static  QueueHandle_t queue = NULL;
typedef struct pkg_prot_frame_t pkg_prot_frame;

uint8_t test_check_sum(uint8_t *buf, uint16_t len, uint16_t *cs_v)
{
    if(buf == NULL)
    {
        DBG_LOG("test check sum pointer is null");
        return false;
    }
    if(len == 0)
    {
        DBG_LOG("test check sum length = 0");
        return false;
    }
    uint16_t cs = 0;
    if(cs_v != NULL)
    {
        cs = *cs_v;
    }
    for(uint16_t i=0;i<len;i++)
    {
        cs += buf[i];
    }
    return cs;
}
#if 1
bool test_parse_handle(uint8_t const *const p_cmd,uint8_t const len,uint8_t type)
{
    if(p_cmd == NULL || len == 0)
    {
        DBG_LOG("test command pointer is null");
        return false;
    }
    bool ret = false;
    pkg_prot_frame_t *cmd_pkg = (pkg_prot_frame_t *)p_cmd;
    uint8_t *p_frot_data = (uint8_t *)(p_cmd+sizeof(pkg_prot_head_t));

    if(cmd_pkg->prot_head.frame_len != len - sizeof(pkg_prot_head_t))
    {
        DBG_LOG("test cmd length is wrong, give len = %d, but cacul len=%d",cmd_pkg->prot_head.frame_len, len - sizeof(pkg_prot_head_t));      
        return false;
    }

    uint16_t head_len = sizeof(pkg_prot_head_t);
    uint16_t give_crc16 = cmd_pkg->prot_head.crc16;
    uint16_t cacul_crc16 = crc16_compute(p_cmd,head_len-2,NULL);
    cacul_crc16 = crc16_compute(p_cmd+head_len,cmd_pkg->prot_head.frame_len,&cacul_crc16);
    if(give_crc16 != cacul_crc16)
    {
        DBG_LOG("test cmd crc16 is wrong, give_crc16=0x%x, cacul_crc16 = 0x%x",give_crc16, cacul_crc16);
        return false;
    }
    protocol_cmd_type_t cmd_type = cmd_pkg->prot_head.cmd_type;
    DBG_LOG("test cmd code = %d", cmd_type);
    switch(cmd_type)
    {
        case PROTOCOL_CMD_TYPE_BT_CONNECT_REQ:
            ret = centr_ble_conn(p_frot_data,cmd_pkg->prot_head.frame_len,type);
            break;
        case PROTOCOL_CMD_TYPE_BT_SCAN_REQ:
            ret = scan_handler(p_frot_data,cmd_pkg->prot_head.frame_len,type);
            //queue = get_scan_queue();
            //xQueueSendToBack(queue,&cmd_pkg->prot_head.frame_len, 10);
            //xQueueSendToBack(queue,&p_frot_data, 10);
            //DBG_LOG("give scan cmd buf addr = 0x%x, len = %d",p_frot_data,cmd_pkg->prot_head.frame_len);
            //ret = true;
            break;
        default:
             DBG_LOG("command parese failed, invalid command type");
            break;
    }
    return ret;
}

#endif
/**
 * @brief 
 * 
 * @param p_msg 
 * @param len 
 * @param type 
 * @return true 
 * @return false 
 */
bool test_parse_ble_msg(uint8_t *p_msg, uint8_t len,uint8_t type)
{
    if(p_msg == NULL || len == 0)
    {
        return false;
    }
    DBG_LOG("test_parse_ble_msg.");
    
    test_ble_msg_t *msg = (test_ble_msg_t *)p_msg;
    DBG_LOG("seq = %d",msg->head.seq);
    DBG_LOG("func_code=%d",msg->head.func_code);
    DBG_LOG("valid data length=%d",msg->head.length);
    DBG_LOG("sizeof(test_ble_msg_head_t)=%d",sizeof(test_ble_msg_head_t));
    uint16_t *p = (uint16_t*)(p_msg+sizeof(test_ble_msg_head_t)+msg->head.length);
    uint16_t give_crc16 = *p;
    uint16_t cacul_crc16 = crc16_compute(p_msg,len-2,NULL);
    if(give_crc16 != cacul_crc16)
    {
        DBG_LOG("give crc16=0x%x, but cacul_crc16=0x%x",give_crc16,cacul_crc16);
        for(uint8_t i=0;i<len/2;i++)
        {
            DBG_LOG("0x%x",*p);
        }
        return false;
    }
    for(uint8_t i=0;i<msg->head.length;i++)
    {
        DBG_LOG("0x%x",msg->data[i]);
    }
    return test_parse_handle(msg->data,msg->head.length,type);
}