
#ifndef TEST_TASK_H
#define TEST_TASK_H

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

void test_creat_task(void);
SemaphoreHandle_t reli_creat_sema(void);
SemaphoreHandle_t reli_creat_stop_sema(void);
void test_start(void);
void test_stop(void);
void test_set_work_mode(uint8_t type);
#endif //TEST_TASK_H