/**
 * @brief 
 * 
 * @file scan.h
 * @date 2018-08-07
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef SCAN_H
#define SCAN_H

#include "ble_gap.h"
//#include "mac.pb.h"
#include "test_upload.h"
#include "app_freertos.h"

//#define SCAN_RESPONSE_VERIFY 1
#define UPLOAD_MAC_NUM_ONCE   5
#define DEVICE_NUMBERS_MAX      200
typedef struct
{
    uint32_t scantime;
    char name[32];
}scan_cmd_t;
typedef struct mac_list_t
{
    ble_gap_addr_t mac;
    struct mac_list_t *next;
}mac_list_t;
typedef struct
{
    uint32_t count;
    //protocol_mac_sruct_t *mac_r;
    mac_list_t *list;
}mac_decode_record_t;
typedef struct
{
    uint32_t count;
    //protocol_mac_sruct_t *mac_r;
    ble_gap_addr_t   **addr;
}mac_record_t;

#if SCAN_RESPONSE_VERIFY
//void test_scan_res(void);
void scan_test(void);
#endif  //SCAN_RESPONSE_VERIFY

bool mac_list_init(void);
bool mac_list_back_insert(ble_gap_addr_t *mac);

void mac_list_test(void);
bool scan_handler(uint8_t *p_cmd_buf, uint32_t len,test_inter_type chn);
void mac_addr_save(ble_gap_addr_t *const mac);
//TaskHandle_t get_scan_handle(void);
TaskHandle_t create_scan_task(void);
QueueHandle_t get_scan_queue(void);
bool get_scan_status(void);
#endif  //SCAN_H
