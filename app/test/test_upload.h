/**
 * @brief 
 * 
 * @param buf 
 * @param len 
 * @param type 
 * @return true 
 * @return false 
 */

#ifndef  TESET_UPLOAD_H
#define  TESET_UPLOAD_H

#include <stdint.h>
#include <stdbool.h>


typedef enum
{
    TEST_INTER_UART = 0,
    TEST_INTER_BLE,
    TEST_INTER_PARALLEL,
    TEST_INTER_MAX
}test_inter_type;

bool test_upload_data(void const *buf, uint8_t len, test_inter_type type);

#endif //TESET_UPLOAD_H
