
#ifndef     USER_APP_IDENTIFY_H
#define     USER_APP_IDENTIFY_H

#include <stdbool.h>
#include "IdentityMsg.pb.h"

#define     DEVICE_MAC_ADDR_LEN             6
#define     DEVICE_MAC_ADDR_CHAR_LEN        DEVICE_MAC_ADDR_LEN*2
#define     DEVICE_ID_MAX_LEN               15

typedef struct
{
    uint32_t msg_token;
    uint32_t msg_index;
    char     dev_id[20];
    uint32_t rsp_code;
}app_identity_t;

typedef struct
{
    protocol_identity_msg_t head;
    char     dev_id[20];
}user_identity_t;
    
//bool user_init_idendity_device_id(void);
bool app_iden_get_device_id_char(char *p_id);
bool app_get_indetity_msg(protocol_identity_msg_t * p_iden_msg);
uint32_t app_get_msg_token(char const * const dev_id, uint16_t const random); 
bool app_add_colon_to_mac_addr(char *const p_mac);

#endif      //USER_APP_IDENTIFY_H

