#include <stdbool.h>
#include <stdint.h>

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "app_error.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "semphr.h"
#include "task.h"
#include "nrf.h"
#include "wtd.h"
#include "app_util_platform.h"
#include "wtd.h"
#include "nrf_nvmc.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "nrfx_wdt.h"
#include "user_app_log.h"
#include "hal_rtc.h"
#include "hal_wdt.h"
#include "scan.h"
#include "test_reliability.h"

//nrfx_wdt_channel_id m_channel_id;

static      TaskHandle_t        wdt_handle;
//static      bool                wdt_event_flag = 0;
//static      uint8_t            *p_ram = (uint8_t *)(USER_RAM_PARAM_START_ADDR);
//static      uint32_t            *p_reset_flag = (uint32_t *)(USER_RAM_PARAM_START_ADDR+4);
//static      uint8_t             reset_cnt = 0;
//static      perform_status      per_sta_t;
//static      uint32_t            timstamp;

//extern      bool get_dfu_flag(void);
//extern      void clear_dft_flag(void);

/**
 * @brief WDT events handler.
 */
void wdt_event_handler(void)
{    
    DBG_LOG("\r\nWTD Rebooting...\r\n"); 

    NVIC_SystemReset();   
}

void user_system_reset(void)
{
    //save parameter;
    //uint8_t len = sizeof(per_sta_t);
    //user_set_param_for_reset();
    //user_save_reset_param_into_flash();
    //reset
    DBG_LOG("\r\nAuto Rebooting...\r\n");
    NVIC_SystemReset(); 
}
/**
 * @brief WDT config.
 */
void hal_config_watch_dog(void)
{
    //uint32_t err_code = NRF_SUCCESS;

    hal_wdt_cfg_t wdt_config;

    wdt_config.timeout = RELOAD_VALUE;

    hal_wdt_init(&wdt_config,wdt_event_handler);
}
void hal_wdt_task(void *arg)
{
    //static uint8_t reset_cnt;
    hal_rtc_init();
    hal_config_watch_dog();
    DBG_LOG("\r\ninit wdt\r\n"); 
    hal_wdt_feed();
    while(1)
    {
        vTaskDelay(RELOAD_VALUE-10);
        hal_wdt_feed();
        DBG_LOG("\r\nfeed watch dog\r\n");
        if(hal_rtc_get_boot_time() >= (28800*3)) 
        {
            if((!get_scan_status()) && (!get_sample_status()))
            {
                user_system_reset();
            }
        }           
          
    }
}

void create_wdt_task(void)
{
    BaseType_t ret;
    ret = xTaskCreate( hal_wdt_task, "wdt", 192, NULL, 1, &wdt_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}

