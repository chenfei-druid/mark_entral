/**
 * @brief 
 * 
 * @file test_uart.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-12
 */
#include "test_uart.h"
#include "hal_uart.h"
#include "nrfx_uarte.h"
#include "user_app_log.h"
#include "test_implement.h"


static  hal_uart_t      *uart = NULL;


/**
 * @brief 
 * 
 */
void test_uart_init(void)
{
    hal_uart_cfg_t uart_config;

    uart_config.baudrate = HAL_UART_BAUDRATE_115200;
    uart_config.parity   = HAL_UART_PARITY_NONE;
    uart_config.rx_buf_size = UART_RX_BUF;
    uart_config.rx_mode = HAL_UART_RX_MODE_BUFFERED;
    uart_config.rx_pin = UART_RX_PIN;
    uart_config.tx_pin = UART_TX_PIN;
    uart_config.rx_timeout_ms = 100;
    uart_config.tx_buf_size = UART_TX_BUF;
    uart_config.tx_mode = HAL_UART_TX_MODE_NOCOPY;
    uart_config.tx_timeout_ms = 100;

    uart = hal_uart_get_instance(0);
    nrfx_uarte_t inst = *(nrfx_uarte_t *)uart->inst;
    DBG_LOG("uart->inst = %d",inst.drv_inst_idx);
    if(uart == NULL)
    {
        DBG_LOG("test uart hal_uart_get_instance failed.");
        return;
    }
    if(uart->ops->init(uart,&uart_config) != HAL_ERR_OK)
    {
        DBG_LOG("test uart init failed.");
        return;
    }
    if(uart->ops->set_rx_timeout(uart,UART_RX_TIMEOUT) != HAL_ERR_OK)
    {
        DBG_LOG("test uart set rx timeout failed.");
        return;
    }
    if(uart->ops->set_rx_enable(uart,1) != HAL_ERR_OK)
    {
        DBG_LOG("test uart set_rx_enable failed.");
        return;
    }
    DBG_LOG("test uart initialized ok.");
}

/**
 * @brief  close uart current instrance
 * 
 */
void test_close_uart(void)
{
    uart->ops->deinit(uart);
}

uint32_t test_uart_trans(void const * const buf, uint8_t length)
{
    hal_err_t ret = uart->ops->write(uart,(uint8_t *)buf,length);
    if(ret < 0)
    {
        return 0;
    }
    return length;
}

uint32_t test_uart_receive(void * const buf, uint8_t max_size)
{
    if(buf == NULL)
    {
        DBG_LOG("test_uart_receive pointer is null.");
        return false;
    }
    uint8_t *rx_buf = buf;
    return uart->ops->read(uart,rx_buf,max_size);
}

//end
