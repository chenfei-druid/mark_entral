/**
 * @brief 
 * 
 * @file simple.c
 * @date 2018-08-01
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "dev_id.h"
#include "user_app_log.h"
#include "pb_callback.h"
#include "simple.pb.h"
#include "simple.h"

#include "user_app_identity.h"

#define SIMPLE_VERIRY 0
#if SIMPLE_VERIRY
/**
 * @brief 
 * 
 * @param buf 
 * @param len 
 */
static void verify_rsp_encode(unsigned char * const buf, uint32_t len)
{
    protocol_simple_rsp_t rsp_verify;
    char dev_id[20] = "\0";
    memset(dev_id,0,sizeof(dev_id));

    //rsp_verify.DevId.funcs.decode = &user_app_decode_repeated_var_string;
    //rsp_verify.DevId.arg = dev_id;
 
    rsp_verify.Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    rsp_verify.Head.DeviceID.arg = dev_id;

    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(buf,len);
    bool res = pb_decode(&i_stream,protocol_simple_rsp_fields,&rsp_verify);
    if(res)
    {

    }
}
#endif  //SIMPLE_VERIRY
/**
 * @brief 
 * 
 * @param ret 
 * @param chn 
 * @return uint32_t 
 */
uint32_t simple_response(protocol_status_type_t status,test_inter_type chn)
{
    protocol_simple_rsp_t rsp;
    unsigned char rsp_buf[50];
    char dev_id[15];
    //user_identity_t iden;

    memset(rsp_buf,0,sizeof(rsp_buf));
    memset(dev_id,0,sizeof(dev_id));

    read_dev_id(dev_id,sizeof(dev_id));
    //rsp.DevId.funcs.encode = &user_app_encode_repeated_var_string;
    //rsp.DevId.arg          = dev_id;
    app_get_indetity_msg(&rsp.Head);
    app_iden_get_device_id_char(dev_id);

    rsp.has_Head = 1;
    rsp.Head.has_RspCode = 1;
    rsp.Head.MsgIndex = 12;
    rsp.Head.MsgToken = 12345678;
    rsp.Head.RspCode = 26;

    rsp.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    rsp.Head.DeviceID.arg = dev_id;

    rsp.Status             = status;

    pb_ostream_t            m_stream ;  
    bool res;  

    m_stream = pb_ostream_from_buffer(rsp_buf,sizeof(rsp_buf));
    res = pb_encode(&m_stream,protocol_simple_rsp_fields,&rsp);

    if(res)
    {
        DBG_LOG("simple_response encode successed\r\n");
    #if VERIFY
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // VERIFY    
        //test_upload_data(rsp_buf,m_stream.bytes_written,chn);
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("selftest response encode failed\r\n");
        return 0;
    }
}


