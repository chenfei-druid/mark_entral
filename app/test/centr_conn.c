/**
 * @brief 
 * 
 * @file test_centr_cmd_conn.c
 * @date 2018-08-02
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_druid_svc.h"
#include "dev_id.h"
#include "simple.h"
#include "pb_callback.h"
#include "centr_conn.h"
#include "ble_conn.pb.h"
#include "HexStr.h"
#include "mac.pb.h"
#include "user_app_identity.h"
#include "ble_trans.h"
#include "test_reliability.h"

#define S            1000
#define CONNECTION_TIMEOUT  30
#define TEST_VERIFY 1
static uint16_t conn_handle = 0xffff;

/**
 * @brief 
 * 
 * @param chn 
 */
void res_fail(test_inter_type chn)
{
    protocol_status_type_t status;
    status = PROTOCOL_STATUS_TYPE_FAILED;
    simple_response(status,chn);
}
/**
 * @brief 
 * 
 * @param p_pro_req 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_ble_conn_req_t * p_pro_req, 
                        char * const mac,
                        uint8_t  *  p_buf,
                        uint32_t const len)
{
    bool res;
    char id[15];
    memset(id,0,sizeof(id));
    pb_istream_t i_stream ;
    memset((uint8_t *)&i_stream,0,sizeof(i_stream));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    p_pro_req->MAC.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->MAC.arg          = mac;

    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_ble_conn_req_fields,p_pro_req);
    if(res == true)
    {
        DBG_LOG("command tester ble connect decode successed\r\n");

        return true;
    }
    else
    {
        DBG_LOG("command tester ble connect decode failed\r\n");
        return false;
    }  
}

/**
 * @brief 
 * 
 * @param mac 
 * @return true 
 * @return false 
 */
/*
bool connect_slave(void)
{
    if(command_type == PROTOCOL_CMD_TYPE_CONNECT)
    {
        if(ble_connect_slave(NULL,NULL,60000) != true)
        {
            return false;
        }
    }
    else if(command_type == PROTOCOL_CMD_TYPE_DISCONNECT)
    {
        if(ble_disconnect_slave(conn_handle) != true)
        { 
            return false ;
        }
    }
    else
    {
        DBG_LOG("the connect command is invalid!\r\n");
    }
    return true;
}
*/

/**
 * @brief 
 * 
 * @param mac 
 * @return true 
 * @return false 
 */
bool connect(const char * const mac)
{
    if(mac == NULL)
    {
        return false;
    }
    uint8_t mac_buf[MAC_LEN];
    StrToHex(mac_buf,(char *)mac,MAC_LEN);
    //vTaskDelay(100);
#if TEST_VERIFY
    if(ble_connect_slave(NULL,NULL,CONNECTION_TIMEOUT*S) != true)
#else
    if(ble_connect_slave(&conn_handle,mac_buf,60000) != true)
#endif
    { 
        return false ;
    }
    resume_reli_task();
    return true;
}
/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
static bool disconnect(void)
{

    if(ble_disconnect_slave(conn_handle) != true)
    { 
        return false ;
    }
    return true;
}
/**
 * @brief 
 * 
 * @param cmd_type 
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(protocol_cmd_type_t cmd_type, char * const mac,test_inter_type chn)
{
    bool ret = 0;
    //uint32_t dt = 0;
    if(mac == NULL)
    {
        return false;
    }
    //memset(s_mac,0,sizeof(s_mac));
    switch(cmd_type)
    {
        case PROTOCOL_CMD_TYPE_CONNECT:
            DBG_LOG("command to connect slave\r\n");
            //StrToHex(s_mac,mac,MAC_LEN);
            //command_type = PROTOCOL_CMD_TYPE_CONNECT;
            //s_mac[MAC_LEN] = 0;
            //ret = true; 
            ret = connect(mac);             
            break;
        case PROTOCOL_CMD_TYPE_DISCONNECT:
            DBG_LOG("command to disconnect slave\r\n");
            //command_type = PROTOCOL_CMD_TYPE_DISCONNECT;
            //s_mac[MAC_LEN] = 0xff;
            //ret = true;
            ret = disconnect();
            break;
        default: 
            ret = false;
            DBG_LOG("the cmd of get device message is invalid\r\n");
            break;
    }
    protocol_status_type_t status;
    status = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
    simple_response(status,chn);
    return ret ;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool centr_ble_conn(uint8_t *p_cmd_buf,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_ble_conn_req_t proto_cmd;
    char mac[15];
    memset(mac,0,sizeof(mac));
    if(cmd_decode(&proto_cmd,mac,p_cmd_buf,cmd_length) != true)
    {
        return false;
    }
    ret = cmd_handler(proto_cmd.Type,mac,chn);
    return ret;
}
#if CENTR_CONN_VERIFY
static void verify_req(void)
{
    protocol_ble_conn_req_t cmd;
    uint8_t req_buf[50] = {0};
    char mac[15] = "E3982105714E\0";

    read_dev_id(mac,sizeof(mac));
    
    
    cmd.has_Head = 1;
    //cmd.Head.MsgIndex = 13;
    //cmd.Head.Timestamp = 123456702;
    cmd.Type = PROTOCOL_CMD_TYPE_CONNECT;

    cmd.MAC.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.MAC.arg          = mac;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg = mac;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_ble_conn_req_fields,&cmd);
    
    if(res)
    {
        centr_ble_conn(req_buf,m_stream.bytes_written,TEST_INTER_UART);
    }
}
void pb_verify(void)
{
    //protocol_ble_conn_req_t cmd;
    //uint8_t req_buf[50] = {0};
    //char mac[15] = "E3982105714E\0";
    //char mac[12];
    //connect(mac);
    //cmd_handler(PROTOCOL_CMD_TYPE_CONNECT,mac,TEST_INTER_BLE);
    //centr_ble_conn(req_buf,10,TEST_INTER_UART);
    //centr_ble_conn(NULL,0,TEST_INTER_UART);
    verify_req();
    //bool ret = false;
    //response(ret,chn);
}
void connect_test(void)
{
    char mac[12] = "1234567890\0";
    connect(mac);
}
#endif

