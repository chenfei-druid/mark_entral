#ifndef     USER_COMMON_ALG_H
#define     USER_COMMON_ALG_H

#include    "stdint.h"
#include    <stdbool.h>

int user_app_rank_min_to_max_32(int32_t *a,int n);
int user_app_rank_min_to_max_16(int16_t * const a,int n) ;
int digit(int a);
bool user_string_compare(char const * const str1, char  const * str2);
bool user_char_to_lower(char *buf);
#endif      //USER_COMMON_ALG_H

