#ifndef USER_DATA_PKG_H
#define USER_DATA_PKG_H


#include <stdint.h>
#include <stdbool.h>
//struct  pkg_unit_trans_t    pkg_unit_trans;

#define     SECOND(x)        (x * 1000)
#define     MINUTE(x)        (x * 60 * 1000)

#define         NB_UINT_TRANS_MAX_LEN_DEFAULT       20
#define         BLE_UNIT_TRANS_MAX_LEN_DEFAULT      256

#define         USER_PACKAGE_MAX_LEN                2000

//protocol internal head
#define         PROT_VER                            1       //fixed
#define         VENDOR_ID                           1       //fixed
#define         FUNCTION_CODE                       1       //0x01-0x7f
#define         OPTION_ENCRYPTION                   0       //1: if secret , 0: not secret

//protocal external head
#define         USER_COMMON_SEQ                       1       //0x01-0xff
#define         USER_COMMON_CODE                      1       //0X01
typedef enum
{
    COMMUNICATION_INTERFACE_TYPE_MIN = 0,
    COMMUNICATION_INTERFACE_TYPE_NB,                //the packets number is LORA_PACKET_SIZE
    COMMUNICATION_INTERFACE_TYPE_BLE,                     //the packets number is BLE_PACKET_SIZE
    COMMUNICATION_INTERFACE_TYPE_MAX
}trans_interface_type;

typedef enum 
{
    HEAD_CMD_MIDDLE_PACKET   = 0x10,        //the middle packet of transmission data
    HEAD_CMD_FIRST_PACKET    = 0x11,        //the first packet of transmission data   
    HEAD_CMD_LAST_PACKET     = 0x12,         //the last packet of transmission data
    HEAD_CMD_SINGLE_PACKET   = 0x13
}pkg_unit_head_cmd;

typedef enum
{
    NB_PACKET_SIZE = NB_UINT_TRANS_MAX_LEN_DEFAULT,         //the unit-packet bytes of LORA transmission
    BLE_PACKET_SIZE  = BLE_UNIT_TRANS_MAX_LEN_DEFAULT,                   //the unit-packet bytes of BLE transmission
}pkg_unit_frame_size;

/*
* _____________________________________________________________________________________________________________|___________________
*----------------------------------------head------------------------------------------------------------------|
* | protocal-version | vendor-id | command-type |   data-len   | encryption-select | reserve |   crc16         | protobuf-data
* |  accumulation    |   start   |              |  protobuf    |                   |         |caculate all     |
* |    from 1        |  from 1   |              | data length  |                   |         |data except self |
* ________________________________________________________________________________________________________________________________
*
*/ 
typedef struct
{
    uint16_t    prot_version;
    uint16_t    vendor_id;
    uint16_t    cmd_type;
    uint16_t    frame_len;
    uint8_t     option_encryption;
    uint8_t     reserve;
    uint16_t    crc16;
}pkg_prot_head_t;

typedef struct
{
    pkg_prot_head_t     prot_head;
    uint8_t             *p_prot_data;
}pkg_prot_frame_t;

/*
* _____________________________________________________________________________________________________________|___________________
*----------------------------------------head------------------------------------------------------------------|
* | frame number | func code cmd |   data-len              |    data       |        crc16         | 
* |    from 1    |               | only data filed length  |    ...        | all data except self                | 
* ________________________________________________________________________________________________________________________________
*
*/ 
typedef struct
{
    uint8_t     sequence;                        //the sequence filed in the data struct, it will be change continous with adding 1 of next complet data
    uint8_t     func_code;                        //the command filed in the data struct, it is fixed.
    uint16_t    data_len;                   //the datafiled length, not including seq, cmd and crc
}pkg_final_frame_head_t;
typedef struct 
{
    pkg_final_frame_head_t  head;
    //uint8_t     datafiled[1500];            //the valid data of accelermeter and the channel for reconglizing completeness of packet
    pkg_prot_frame_t        *frame;
    uint16_t    crc16;                        //verify all data
}pkg_final_frame_trans_t;


typedef struct
{
    uint8_t     *p_buff;                    //the complete data will be send buff
    uint16_t    remain_length;
    uint8_t     attribute;                  //to distinuish the data is  NB_DATA_TYPE or BLE_DATA_TYPE
}pkg_trans_buffer_t;

/**
* split the final data frame when it's lager than once send data len by ble or other interface, 
* the actual length @ref pkg_unit_frame_size. The first byte is used to identify of unit frame,
* @ref pkg_unit_head_cmd
*/
typedef struct 
{
    uint8_t     head;                        //the sequence filed in the data struct, it will be change continous with adding 1 of next complet data
    uint8_t     *datafiled;                 //the valid data of accelermeter and the channel for reconglizing completeness of packet
}pkg_unit_frame_t;

typedef struct
{
    pkg_unit_frame_t    frame;
    uint16_t            length;
}pkg_unit_trans_t;

uint16_t user_add_head_to_protbuf(pkg_prot_frame_t * p_prot_frame_t, 
                                  uint8_t * const protbuf_encode_data, 
                                  int16_t const protbuf_length,
                                  uint16_t cmd_type
                                  );
uint16_t user_package_final_frame(pkg_final_frame_trans_t * p_final_frame_t, 
                                  pkg_prot_frame_t * p_prot_frame_t, 
                                  const int16_t frame_len,
                                  uint8_t sequence,
                                  uint8_t func_code
                                  );   
uint32_t user_put_final_frame_into_buf(uint8_t *p_buf, pkg_final_frame_trans_t * p_final_frame_t);
uint32_t user_ble_data_packge(uint8_t *const p_dst, int32_t out_buf_size, uint8_t *p_src, int32_t byte_len, uint16_t cmd_type,uint8_t sequence, uint8_t final_cmd);
uint16_t user_get_sub_pkg_from_cache(uint8_t const * const src_buf, 
                                     uint8_t *dst_buf, 
                                     uint16_t const curren_len,
                                     uint16_t const total_len,
                                     uint16_t const split_pkg_len);  
uint32_t user_nb_data_packge(uint8_t *const p_dst, int32_t out_buf_size, uint8_t *p_src, int32_t byte_len, uint16_t cmd_type);
uint32_t user_data_packge(  uint8_t *const p_dst, int32_t out_buf_size, uint8_t *p_src, int32_t byte_len, 
                            uint16_t cmd_type, uint8_t sequence, uint8_t func_code,
                            trans_interface_type chan_type
                         );                                     
#endif

