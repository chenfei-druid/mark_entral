#include <stdint.h>
#include "nrfx_gpiote.h"
#include "test_task.h"
#include "hal_cpu.h"
#include "hal_uart.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
//#include "semphr.h"
//#include "task.h"
#include "user_app_log.h"
//#include "hal_rtc.h"
//#include "nrfx_uarte.h"
#include "test_implement.h"
#include "record.h"
//#include "user_data_pkg.h"
#include "hal_pin.h"
#include "test_task.h"
#include "test_parallel.h"
#include "ble_druid_svc.h"
#include "ble_trans.h"
#include "test_uart.h"


/**--------------------------------------defines--------------------------------------------**/
#define MAC_ADDR_LEN    6
#define LORA_ENABALE    0
#define GPS_ENABLE      0
#define NET_ENABLE      0

static  char const success[] = "SUCCESS\0";
static  char const fail[] = "SUCCESS\0";

static  test_inter_type cmd_inter_flag = TEST_INTER_UART;


/**--------------------------------------function--------------------------------------------**/
/**
 * @brief 
 * 
 * @param buf 
 * @param len 
 * @param type 
 * @return true 
 * @return false 
 */

/**
 * @brief 
 * 
 * @param type 
 */
void test_answer_ok(test_inter_type type)
{
    test_upload_data(success,7,type);
}
/**
 * @brief 
 * 
 * @param type 
 */
void test_answer_err(test_inter_type type)
{
    test_upload_data(fail,4,type);
}

bool test_give_cmd_to_slave(uint8_t *p_cmd,uint16_t cmd_len)
{
    if(cmd_inter_flag < TEST_INTER_MAX)
    {
        test_upload_data(p_cmd,cmd_len,cmd_inter_flag);
    }
    else
    {
        DBG_LOG("give command failed");
        return false;
    }
    DBG_LOG("give command successed");
    return true;
}
/**
 * @brief 
 * 
 * @param p_buf 
 * @param size 
 * @return uint32_t 
 */
uint32_t test_wait_msg_from_device(uint8_t *p_buf,uint16_t size)
{
    uint16_t poll_cnt = 0;
    uint16_t len = 0;
    if(p_buf == NULL)
    {
        DBG_LOG("wait msg from device pointer is null");
        return 0;
    }
    if(cmd_inter_flag == TEST_INTER_UART)
    {        
        DBG_LOG("wait msg from device via ble");
        do
        {
            len = test_uart_receive(p_buf,size);
            poll_cnt++;
        }while((len == 0) && (poll_cnt < 20)); //10S
    }
    else if(cmd_inter_flag == TEST_INTER_BLE)
    {
        DBG_LOG("wait msg from device via uart");
        do
        {
            len = ble_c_receive(p_buf,size,1000);
            poll_cnt++;
        }while((len == 0) && (poll_cnt < 1)); //10S
    }
    else
    {
        return 0;
    }
    return len;
}
/**
 * @brief 
 * 
 * @param p_buf 
 * @param lengh 
 * @return true 
 * @return false 
 */
bool test_cmd_for_target_process(uint8_t *p_buf, uint16_t lengh)
{
    if(test_give_cmd_to_slave(p_buf, lengh) != true)
    {
        DBG_LOG("transmit command failed");
        test_answer_err(TEST_INTER_PARALLEL);
        return false;
    }
    uint16_t ret_len = test_wait_msg_from_device(p_buf,CMD_BUF);
    if(ret_len < 0)
    {
        DBG_LOG("wait msg from device is faield");
        test_answer_err(TEST_INTER_PARALLEL);
        return false;
    }
    test_upload_data(p_buf,ret_len,TEST_INTER_PARALLEL);
    return true;
}
/**
 * @brief 
 * 
 * @param type 
 * @return true 
 * @return false 
 */
bool test_self_test(test_inter_type type)
{
    if(ble_scan_slave(10*1000) != true)
    {
        test_answer_err(type);
        DBG_LOG("self test failed");
        return false;
    }
    test_answer_ok(type);
    DBG_LOG("self test success");
    return true;
}

bool test_send_cmd(test_inter_type type)
{
    cmd_inter_flag = TEST_INTER_BLE;
    test_answer_ok(type);
    DBG_LOG("set cmd channel");
    return true;
}

bool test_read_data(uint8_t * p_data,uint8_t length,test_inter_type type)
{
    //test_give_cmd_to_slave(p_data,length);
    //test_wait_msg_from_device(p_data,CMD_BUF);
    if(test_cmd_for_target_process(p_data,length) != true)
    {
        return false;
    }
    test_answer_ok(type);
    return true;
}

bool test_set_io(uint8_t *cmd,test_inter_type type)
{
    pin_ctl_t *io = (pin_ctl_t *)cmd;
    hal_err_t ret;
    //the bit7 is pin level, bit0~bit6 is pin number
    ret = hal_pin_write(io->pin,io->level);
    if(ret > 0)
    {
        test_answer_ok(type);
    }
    else
    {
        test_answer_err(type);
    }
    return true;
}
bool test_ble_connect(uint8_t *mac,test_inter_type type)
{
    if(ble_connect_slave(mac,10*1000) != true)
    {
        test_answer_err(type);
        return false;
    }
    test_answer_ok(type);
    return true;
}
bool test_ble_disconnect(test_inter_type type)
{
    ble_disconnect_slave();
    test_answer_ok(type);
    return true;
}
