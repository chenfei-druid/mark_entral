#ifndef     USER_APP_LOG_H
#define     USER_APP_LOG_H

#include    "SEGGER_RTT.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#if NRF_LOG_ENABLED
#define    NRF_UART_LOG     1
#endif

#define     SEGGER_LOG_ENABLED  1

#if NRF_UART_LOG
#define  DBG_LOG(...)       NRF_LOG_INFO(__VA_ARGS__)

#elif   SEGGER_LOG_ENABLED
#define  DBG_LOG(...);      SEGGER_RTT_printf(0,__VA_ARGS__);SEGGER_RTT_printf(0,"\r\n"); //vTaskDelay(1);
#define  DBG_D(...);      SEGGER_RTT_printf(0,__VA_ARGS__);            

#else
#define  DBG_LOG(...)       ;

#endif

#endif  //USER_APP_LOG_H

