/**
 * @brief 
 * 
 * @file test_parse.h
 * @date 2018-08-03
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TEST_PARSE_H
#define TEST_PARSE_H

#include <stdint.h>
#include <stdbool.h>
typedef enum
{
    TEST_CMD_READ_MAC = 0,
    TEST_CMD_READ_IMEI,         //1
    TEST_CMD_READ_IMSI,
    TEST_CMD_WRITE_SN,
    TEST_CMD_READ_SN,
    TEST_CMD_BT_MODE,
    TEST_CMD_BLE_LOOP_BACK,    
    TEST_CMD_LOW_SPEED_MODE,
    TEST_CMD_HIGH_SPEED_MODE,
    TEST_CMD_LORA_MODE,
    TEST_CMD_LORA_FRQ_SET,      //10
    TEST_CMD_LORA_POWER_SET,
    TEST_CMD_LORA_SPEED,
    TEST_CMD_LORA_SLEEP,
    TEST_CMD_LORA_LOOP_BACK,
    TEST_CMD_WAN_MODE,
    //TEST_CMD_WAN_LOOP_BACK,     //16
    TEST_CMD_RAND_DATA,
    TEST_CMD_GPS_MODE,
    TEST_CMD_GPS_START,
    TEST_CMD_GPS_CNR,
    TEST_CMD_GPS_STOP,
    TEST_CMD__MIN_POWER_MODE,
    TEST_CMD__MAX_POWER__MODE,
    
    TEST_CMD_SELF_TEST,         //1
    TEST_CMD_SEND_CMD,
    TEST_CMD_READ_DATA,
    TEST_CMD_IO_ON_OFF,
    TEST_CMD_BT_CONNECT,
    TEST_CMD_BT_DISCONNECT,    
    TEST_CMD_MAX,
}test_cmd_type;
typedef struct
{
    uint8_t     start;      //command start   
    uint8_t     type;       //command type
    uint16_t    length;
    uint16_t    crc16;

}test_cmd_head_t;

/*
typedef struct
{
    uint16_t crc16;      //command start
    uint16_t end;       //command type
}test_cmd_end_t;

typedef struct
{
    test_cmd_head_t    head;
    uint8_t                 *data;
    test_cmd_end_t     end;
}test_cmd_t;*/
typedef struct
{
    test_cmd_head_t head;
    uint8_t     data[256];
}test_cmd_t;

typedef struct
{
    uint8_t seq;
    uint8_t func_code;
    uint16_t length;
}test_ble_msg_head_t;
typedef struct
{
    test_ble_msg_head_t     head;
    uint8_t data[256];
}test_ble_msg_t;

bool test_parse_pkg_handle(uint8_t *p_cmd,uint8_t len,uint8_t type);
bool test_parse_ble_msg(uint8_t *p_msg, uint8_t len,uint8_t type);
bool test_parse_handle(uint8_t const *const p_cmd,uint8_t const len,uint8_t type);
#endif //TEST_PARSE_H

