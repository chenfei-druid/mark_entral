/**
 * @brief 
 * 
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_druid_svc.h"
#include "dev_id.h"
#include "simple.h"
#include "pb_callback.h"
#include "HexStr.h"
#include "mac.pb.h"
#include "user_app_identity.h"
#include "ble_trans.h"
#include "scan.h"
#include "scan.pb.h"
#include "user_data_pkg.h"
#include "define.pb.h"
#include "hal_pin.h"

#define S            1000
#define SCAN_INDEX_LED  8
static TaskHandle_t  scan_handle = NULL;
static QueueHandle_t        queue_scan = NULL;
static SemaphoreHandle_t    sema_scan_end = NULL; 
static struct mac_list_t *m_mac_list = NULL;
static uint16_t mac_addr_num = 0;
static char slave_name[32] = {0};
static uint32_t scan_time = 0;
static bool scan_status = 0;
/**
 * @brief Get the scan status object
 * 
 * @return true 
 * @return false 
 */
bool get_scan_status(void)
{
    return scan_status;
}
/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
bool mac_list_init(void)
{
    //m_mac_list.next = NULL;
    if(m_mac_list != NULL)
    {
        return true;
    }
    vPortFree(m_mac_list);
    m_mac_list = (mac_list_t *)pvPortMalloc(sizeof(mac_list_t));
    if(m_mac_list == NULL)
    {
        DBG_LOG("link list init failed\r\n");
        return false;
    }
    m_mac_list->next = NULL;
    DBG_LOG("link list init successed\r\n");
    return true;
}
/**
 * @brief 
 * 
 * @param mac 
 * @return mac_list_t* 
 */
static mac_list_t * mac_list_search(ble_gap_addr_t *mac)
{
    mac_list_t *p = m_mac_list;
    for(;p != NULL;p=p->next)
    {
        if(memcmp(mac->addr,p->mac.addr,6) == 0)
        {
            return p;
        }
    }
    return NULL;
}
/**
 * @brief 
 * 
 * @param mac 
 * @return true 
 * @return false 
 */
bool mac_list_back_insert(ble_gap_addr_t *mac)
{
    if(m_mac_list == NULL)
    {
        if(mac_list_init() != true)
        {
            return false;
        }
    }
    if(mac_list_search(mac) != NULL)
    {
        DBG_LOG("The scan result is exist\r\n");
        return true;
    }
    mac_list_t *node = NULL;
    mac_list_t *p = NULL;
    for(p = m_mac_list;p->next != NULL;p=p->next)
    {
        ;
        //DBG_LOG("linklist address p = 0x%x, p->next = 0x%x\r\n",p,p->next);
    }
    node = (mac_list_t *)pvPortMalloc(sizeof(mac_list_t));;
    if(node == NULL)
    {
        DBG_LOG("apply memory for new list node failed\r\n");
        return false;
    }
    node->mac.addr_id_peer = mac->addr_id_peer;
    node->mac.addr_type    = mac->addr_type;
    memcpy(node->mac.addr,mac->addr,6);

    p->next = node;
    node->next = NULL;
    mac_addr_num++;
    DBG_LOG("linklist address p = 0x%x, new apply node = 0x%x\r\n",p,node);
    return true;
}
bool mac_list_delete(mac_list_t *node)
{
    if(node == NULL)
    {
        return false;
    }
    m_mac_list->next = node->next;
    node->next = NULL;
    vPortFree(node);
    return true;
}
/**
 * @brief 
 * 
 * @param mac 
 */
void mac_addr_save(ble_gap_addr_t *const mac)
{
    if(mac_list_back_insert(mac) != true || (mac_addr_num >= DEVICE_NUMBERS_MAX))
    {
        scan_stop();
        xSemaphoreGiveFromISR(sema_scan_end,NULL);
        return;
    }
}
static bool encode_repeated_struct(pb_ostream_t *stream, const pb_field_t fields[],  void * const *src_struct)
{   
    bool res;
    
    mac_record_t *m_head_t = (mac_record_t *)*src_struct;
    //protocol_mac_sruct_t    *record_t        = m_head_t->mac_r;
    ble_gap_addr_t **addr = m_head_t->addr;
    protocol_mac_sruct_t    record_t;
    char mac_str[13];
    for(uint8_t i=0; i<m_head_t->count; i++)
    {
        DBG_LOG("encode mac struct number %d of %d successed\r\n\0",i,m_head_t->count);
        memset(mac_str,0,sizeof(mac_str));
        HexToStr(mac_str,addr[i]->addr,sizeof(addr[i]->addr));
        record_t.Addr.funcs.encode = &user_app_encode_repeated_var_string;
        record_t.Addr.arg          = mac_str;
        record_t.has_AddrIdPeer = 1;
        record_t.AddrIdPeer     = addr[i]->addr_id_peer;
        record_t.has_AddrType   = 1;
        record_t.AddrType       = addr[i]->addr_type;

        if (!pb_encode_tag_for_field(stream, fields))
        {
            DBG_LOG("encode mac tag failed\r\n\0");
            return false;
        }
        //res = pb_encode_submessage(stream, protocol_mac_sruct_fields, &record_t[i]);
        res = pb_encode_submessage(stream, protocol_mac_sruct_fields, &record_t);
        if(res == false)
        {
            DBG_LOG("encode mac struct failed\r\n\0");
            break;
        }     
    }
    DBG_LOG("encode mac struct is %s\r\n", res? "successed":"failed");
    
    return res;
}
#if SCAN_RESPONSE_VERIFY
static bool decode_repeated_struct(pb_istream_t *stream, const pb_field_t *field, void **src_struct)
{
    bool ret = false;
    //static int decode_struct_cnt = 0;
    mac_decode_record_t    *m_head_t = (mac_decode_record_t *)*src_struct;   
    //protocol_mac_sruct_t *record_t = m_head_t->mac_r; 
    protocol_mac_sruct_t record;
    mac_list_t *list =   m_head_t->list;
    char mac_addr[13];
    m_head_t->count = 0;
    mac_list_t *p = list;   
    do
    {
        memset(mac_addr,0,sizeof(mac_addr));
        record.Addr.funcs.decode = &user_app_decode_repeated_var_string;
        record.Addr.arg          = mac_addr;
        ret = pb_decode(stream,protocol_mac_sruct_fields,&record);
        //ret = pb_dec_submessage(stream,protocol_mac_sruct_fields,&record);
        DBG_LOG("decode mac struct %s count %d\r\n", ret? "successed":"failed",m_head_t->count);
        if(ret)
        {           
            mac_list_t *node = NULL;
            //vPortFree(node);
            node = (mac_list_t *)pvPortMalloc(sizeof(mac_list_t));
            if(record.has_AddrIdPeer)
            {
                node->mac.addr_id_peer = record.AddrIdPeer;
            }
            else
            {
                node->mac.addr_id_peer = 0;
            }
            if(record.has_AddrType)
            {
                node->mac.addr_type = record.AddrType;
            }
            else
            {
                node->mac.addr_type = 0;
            }
            StrToHex(node->mac.addr,mac_addr,sizeof(node->mac.addr));
            p->next = node;
            p = p->next;
        }
        m_head_t->count++;
        if(m_head_t->count >= (UPLOAD_MAC_NUM_ONCE+1))//UPLOAD_MAC_NUM_ONCE)
        {
            break;
        }
    }while(ret);
    //vTaskDelay(5000);
    return ret;
}
/**
 * @brief 
 * 
 * @param en_buf 
 * @param len 
 */
void verify_rsp_encode(uint8_t * en_buf, uint16_t len)
{
    protocol_ble_mac_req_t veriry_mac;
    char id[15];
    //char mac_addr[15];
    //protocol_mac_sruct_t mac_record[UPLOAD_MAC_NUM_ONCE];
    //mac_decode_record_t mac_msg;
    //char mac_addr[UPLOAD_MAC_NUM_ONCE][6*2+1];
    mac_decode_record_t decode_msg;

    memset(id,0,sizeof(id));
    //memset(mac_addr,0,sizeof(mac_addr));
    decode_msg.list = (mac_list_t *)pvPortMalloc(sizeof(mac_list_t));

    veriry_mac.Head.DeviceID.funcs.decode    = &user_app_decode_repeated_var_string;
    veriry_mac.Head.DeviceID.arg             = id;

        veriry_mac.MAC.funcs.decode          = &decode_repeated_struct;
        veriry_mac.MAC.arg                   = &decode_msg;
    
    //veriry_mac.MAC.Addr.funcs.decode          = &user_app_decode_repeated_var_string;
    //veriry_mac.MAC.Addr.arg                   = mac_addr;

    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(en_buf,len);
    bool res = pb_decode(&i_stream,protocol_ble_mac_req_fields,&veriry_mac);
    if(res)
    {
            ;
    }
}
#endif 
int encode_mac_msg(uint8_t *pb_buf, uint32_t buf_size,ble_gap_addr_t *addr[],uint32_t record_size)
{
    if(pb_buf == NULL || addr == NULL)
    {
        DBG_LOG("encode_mac_msg pointer is NULL\r\n");
        return 0;
    }
    protocol_ble_mac_req_t proto_mac;
    char dev_id[15];
    //char mac_addr[15];
    //uint8_t pb_buf[500];
    //protocol_mac_sruct_t mac_record[UPLOAD_MAC_NUM_ONCE];
    mac_record_t mac_msg;
    //char mac_addr[UPLOAD_MAC_NUM_ONCE][6*2+1];

    memset(dev_id,0,sizeof(dev_id));
    //memset(mac_addr,0,sizeof(mac_addr));
    memset(pb_buf,0,sizeof(pb_buf));
    proto_mac.has_Head = 1;

    proto_mac.Head.MsgIndex = 1;
    proto_mac.Head.RspCode = 123;

    app_get_indetity_msg(&proto_mac.Head);
    app_iden_get_device_id_char(dev_id);

    proto_mac.Head.DeviceID.funcs.encode    = &user_app_encode_repeated_var_string;
    proto_mac.Head.DeviceID.arg             = dev_id;
    
    //mac addr message;
    mac_msg.count    = record_size;
    mac_msg.addr     = addr;

    proto_mac.MAC.funcs.encode = &encode_repeated_struct;
    proto_mac.MAC.arg          = &mac_msg;

    pb_ostream_t            m_stream ;  
    bool ret;
    m_stream = pb_ostream_from_buffer(pb_buf,buf_size);
    ret = pb_encode(&m_stream,protocol_ble_mac_req_fields,&proto_mac);
    //vTaskDelay(5000);
    if(ret)
    {
        DBG_LOG("response mac addr encode successed\r\n");
    #if SCAN_RESPONSE_VERIFY
        verify_rsp_encode(pb_buf,m_stream.bytes_written);
        return true;
    #else   
        return m_stream.bytes_written; 
    #endif  // SCAN_RESPONSE_VERIFY 
    }
    else
    {
        DBG_LOG("response mac addr encode failed\r\n");
        return 0;
    }
}
/**
 * @brief 
 * 
 * @param addr 
 * @return int 
 */
int response_mac_addr(ble_gap_addr_t *addr[],uint32_t record_size)
{
    if(addr == NULL)
    {
        DBG_LOG("response_mac_addr pointer is NULL\r\n");
        return 0;
    }
    int len = 0;
    uint8_t pb_buf[UPLOAD_MAC_NUM_ONCE * 25];
    uint8_t send_buf[UPLOAD_MAC_NUM_ONCE * 25+12];
    if(record_size > 0)
    {
        len = encode_mac_msg(pb_buf,sizeof(pb_buf),addr,record_size);
        if(len < 0)
        {
            return 0;
        }
    }
    pkg_prot_frame_t p_prot_frame_t;
    //len = pkg_mac_msg(send_buf,pb_buf);
    len = user_add_head_to_protbuf(&p_prot_frame_t,pb_buf,len,PROTOCOL_CMD_TYPE_BT_SCAN_RSP);
    if(len > sizeof(send_buf))
    {
        return 0;
    }
    memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
    memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
    len = ble_s_pkg_send(send_buf,len);
    return len;
}
/**
 * @brief 
 * 
 * @return int 
 */
int scan_response(void)
{
    mac_list_t *p = NULL;
    ble_gap_addr_t *addr[UPLOAD_MAC_NUM_ONCE];
    uint32_t times = 0;
    p = m_mac_list->next;
    uint8_t record_cnt = 0;
    while(p!= NULL)
    {
        mac_list_t *tp = p;
        for(uint8_t i=0;(i<UPLOAD_MAC_NUM_ONCE) && (tp != NULL);i++)
        {
            addr[i] = (ble_gap_addr_t *)&tp->mac;
            //addr[i].addr_id_peer = tp->mac.addr_id_peer;
            //addr[i].addr_type    = tp->mac.addr_type;
            //memcpy((uint8_t *)addr[i].addr,(uint8_t *)tp->mac.addr,6);
            tp = tp->next;
            record_cnt++;
        }
        if(response_mac_addr(addr,record_cnt) == false)
        {
            DBG_LOG("scan response failed and end, successed times = %d\r\n",times);
            return false;
        }
        times++;
        mac_list_t *temp = NULL;
        for(uint8_t i=0;(i<record_cnt) && (p != NULL);i++)
        {
            temp = p->next; //point to next node, (must not point to p)
            if(mac_list_delete(p) != false)
            {
                //num++;
                p = temp;
                if(mac_addr_num > 0)
                {
                    mac_addr_num--;
                }
            }
            else
            {
                DBG_LOG("scan response delete list failed\r\n");
                return false;
            }
            //DBG_LOG("scan response delete list success, number = %d\r\n",mac_addr_num);
        }
        //p = temp;
        record_cnt = 0;
        //DBG_LOG("response:linklist address p = 0x%x, p->next = 0x%x\r\n",p,p->next);
    }
    if(m_mac_list->next == NULL)
    {
        mac_addr_num = 0;
    }
    DBG_LOG("scan response over\r\n");
    return true;
}

/**
 * @brief 
 * 
 * @param p_name 
 * @param timeout 
 * @return true 
 * @return false 
 */
static bool scan_dev(const char * const p_name,uint32_t const timeout)
{
    //scan_start();
    if(scan_start_name(p_name) != true)
    {
        DBG_LOG("start scan device \"%s\" failed\r\n",p_name);
    }
    DBG_LOG("start scan device \"%s\", wait %dS\r\n",p_name,timeout);
    if(sema_scan_end == NULL)
    {
        sema_scan_end = xSemaphoreCreateBinary();
    }
    if(sema_scan_end == NULL)
    {
        return false;
    }
    hal_pin_write(SCAN_INDEX_LED,1);
    //vTaskDelay(50);
    
    if(xSemaphoreTake(sema_scan_end,timeout*S) == pdPASS)
    {
        if(mac_addr_num >= DEVICE_NUMBERS_MAX)
        {
            DBG_LOG("the scan number is reached limited numbers %d\r\n",DEVICE_NUMBERS_MAX);
        }
    }
    hal_pin_write(SCAN_INDEX_LED,0);
    scan_stop();
    scan_response();
    DBG_LOG("scan over\r\n");
    return true;
}
static bool set_scan_pram(const char * const p_name,uint32_t const timeout)
{
    if(scan_status == 1)
    {
        scan_status = 0;
        xSemaphoreGiveFromISR(sema_scan_end,NULL);
        return true;
    }
    if(strlen(p_name) > sizeof(slave_name))
    {
        return false;
    }
    memset(slave_name,0,sizeof(slave_name));
    if(p_name[0] != '\0')
    {
        if((p_name[0] <=47) || (p_name[0] >= 127))
        {
            return false;
        }
        strcpy(slave_name,p_name);
    }
    if(timeout > 3600)
    {
        return false;
    }
    /*
    if(timeout == 0)
    {
        xSemaphoreGiveFromISR(sema_scan_end,NULL);
        return true;
    }*/
    scan_time = timeout;
    xTaskNotifyGive(scan_handle);
    scan_status = 1;
    DBG_LOG("notify the scan task\r\n");
    return true;
}
//static void excecute_scan_command()
/**
 * @brief 
 * 
 * @param p_scan_cmd 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode( scan_cmd_t * p_scan_cmd,
                        uint8_t  *  p_buf,
                        uint32_t const len)
{
    protocol_ble_scan_req_t p_pro_req;
    bool res;
    char id[15];
    memset(id,0,sizeof(id));
    pb_istream_t i_stream ;
    memset((uint8_t *)&i_stream,0,sizeof(i_stream));

    p_pro_req.Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req.Head.DeviceID.arg = id;

    p_pro_req.Name.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req.Name.arg          = p_scan_cmd->name;

    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_ble_scan_req_fields,&p_pro_req);
    if(res == true)
    {
        p_scan_cmd->scantime = p_pro_req.ScanTime;
        DBG_LOG("command scan decode successed\r\n");
        return true;
    }
    else
    {
        DBG_LOG("command scan decode failed\r\n");
        return false;
    }  
}
/**
 * @brief 
 * 
 * @param p_cmd_buf 
 * @param len 
 * @param chn 
 * @return true 
 * @return false 
 */
bool scan_handler(uint8_t *p_cmd_buf, uint32_t len,test_inter_type chn)
{
    bool ret = false;
    scan_cmd_t scan_cmd;

    memset(&scan_cmd,0,sizeof(scan_cmd));
    if(cmd_decode(&scan_cmd,p_cmd_buf,len) != true)
    {
        return false;
    }
    char *p_name = NULL;
    if(scan_cmd.name[0] != 0)
    {
        p_name = scan_cmd.name;
    }
    //ret = scan_dev(p_name,scan_cmd.scantime);
    set_scan_pram(p_name,scan_cmd.scantime);
    return ret;
}
void scan_task(void *arg)
{
    mac_list_init();    //initialize mac addr memeory linkedlist
    hal_pin_set_mode(SCAN_INDEX_LED,HAL_PIN_MODE_OUT);
    hal_pin_write(SCAN_INDEX_LED,0);
    if(queue_scan == NULL)
    {
        queue_scan = xQueueCreate(2, sizeof(uint32_t));
    }
    xQueueReset(queue_scan);
    while(1)
    {
        ulTaskNotifyTake( pdTRUE,          /* Clear the notification value beforeexiting. */
                          portMAX_DELAY ); /* Block indefinitely. */
        
        scan_dev(slave_name,scan_time);
        /*
        p_cmd = NULL;
        if(xQueueReceive(queue_scan,&cmd_len,portMAX_DELAY) == pdPASS)
        {
            if(cmd_len > 0)
            {
                xQueueReceive(queue_scan,&p_cmd,0);
                DBG_LOG("received scan cmd buf addr = 0x%x, len = %d",p_cmd,cmd_len);
            }
            if(p_cmd != NULL)
            {
                memcpy(cmd_buf, p_cmd,cmd_len);
                scan_handler(cmd_buf,cmd_len,TEST_INTER_BLE);
            }
        }
        */
    }
}
TaskHandle_t create_scan_task(void)
{
    uint32_t ret = xTaskCreate( scan_task, "scan", 1024, NULL, 1, &scan_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    return scan_handle;
}

void suspend_scan_task(void)
{
    vTaskSuspend(scan_handle);
}
void resume_scan_task(void)
{
    vTaskResume(scan_handle);
}
TaskHandle_t get_scan_handle(void)
{
    return scan_handle;
}
QueueHandle_t get_scan_queue(void)
{
    return queue_scan;
}
#if SCAN_RESPONSE_VERIFY
void mac_list_test(void)
{
    ble_gap_addr_t mac;
    mac_list_init();
    for(uint8_t i=0;i<DEVICE_NUMBERS_MAX;i++)
    {
        mac.addr_id_peer = 0;
        mac.addr_type = 1;
        for(uint8_t i=0;i<6;i++)
        {
            mac.addr[i] = rand()%256;
        }
        mac_list_back_insert(&mac);
    }
}
void scan_test(void)
{
    mac_list_test();
    //scan_dev("DEBUT-MRK\0",30);
    scan_response();
    //ble_gap_addr_t mac;
    //mac_addr_save(&mac);
}
/*
void test_scan_res(void)
{
    ble_gap_addr_t addr;
    addr.addr_type = 1;
    addr.addr_id_peer = 0;
    for(uint8_t i=0;i<6;i++)
    {
        addr.addr[i] = rand()%256;
    }
    response_mac_addr(&addr,i);
}*/
#endif

