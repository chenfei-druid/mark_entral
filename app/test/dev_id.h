/**
 * @brief 
 * 
 * @file dev_id.h
 * @date 2018-08-02
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef DEV_ID_H
#define DEV_ID_H

#include <stdint.h>


bool read_dev_id(char * const p_mac, uint32_t size);

#endif  //DEV_ID_H
