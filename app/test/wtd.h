#ifndef WTD_H
#define WTD_H

#define FEED_BUTTON_ID          0                           /**< Button for feeding the dog. */
#define RELOAD_VALUE            65000
#define RESET_FLAG_RAM_SIZE     USER_RAM_PARAM_SIZE

void create_wdt_task(void);
void wdt_event_handler(void);
void user_system_reset(void);
#endif  //WTD_H

