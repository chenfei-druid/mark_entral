/**
 * @brief 
 * 
 * @file test_reliability.h
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-20
 */
#ifndef TEST_RELIABILITY_H
#define TEST_RELIABILITY_H

#include "task.h"

typedef enum
{
    START = 0,
    STOP,
}work_mode;

TaskHandle_t create_sample_task(void);
TaskHandle_t reli_task_delete(void);
void suspend_reli_task(void);
void resume_reli_task(void);
bool get_sample_status(void);
#endif  //TEST_RELIABILITY_H