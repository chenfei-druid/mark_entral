#ifndef TEST_IMPLEMENT_H
#define TEST_IMPLEMENT_H

#include <stdint.h>
#include <stdbool.h>
#include "test_upload.h"
typedef struct
{
    char sn[12];
    uint32_t timestamp;
}sn_record_t;
/*
typedef enum
{
    CMD_VIA_BLE = 0,
    CMD_VIA_UART
}cmd_inter;
*/
typedef struct
{
    uint8_t pin : 7;
    uint8_t level : 1;
}pin_ctl_t;

bool test_cmd_for_target_process(uint8_t *p_buf, uint16_t lengh);
//tester board
bool test_self_test(test_inter_type type);
bool test_send_cmd(test_inter_type type);
bool test_read_data(uint8_t * p_data,uint8_t length, test_inter_type type);
bool test_set_io(uint8_t *cmd,test_inter_type type);
bool test_ble_connect(uint8_t *mac,test_inter_type type);
bool test_ble_disconnect(test_inter_type type);

 #endif //TEST_IMPLEMENT_H
