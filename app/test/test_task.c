/**
 * @brief 
 * 
 * @file test_task.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-10
 */
/**--------------------------------------includes---------------------------------------------**/
#include "test_task.h"
#include "user_app_log.h"
#include "test_reliability.h"
#include "nrfx_gpiote.h"
#include "hal_pin.h"
#include "ble_druid_svc.h"
#include "test_upload.h"
#include "test_parse.h"
#include "centr_conn.h"
#include "ble_trans.h"
#include "scan.h"

#define LED_RUN            9

static  TaskHandle_t    test_handle = NULL;
static  QueueHandle_t       queue_work_mode;
static SemaphoreHandle_t    reli_sema = NULL;
static SemaphoreHandle_t    reli_stop_sema = NULL;

SemaphoreHandle_t reli_creat_sema(void)
{
    reli_sema = xSemaphoreCreateBinary();
    if(reli_sema == NULL)
    {
        return NULL;
    }
    return reli_sema;
}
SemaphoreHandle_t reli_creat_stop_sema(void)
{
    reli_stop_sema = xSemaphoreCreateBinary();
    if(reli_stop_sema == NULL)
    {
        return NULL;
    }
    return reli_stop_sema;
}
void test_gpio_init(void)
{
    if(nrfx_gpiote_init() != NRFX_SUCCESS)
    {
        DBG_LOG("test gpio initialized failed.");
        return;
    }
    DBG_LOG("test gpio initialized ok.");
}
void test_task_handle(void *arg)
{   
    DBG_LOG("+++++++++++++++ test task startup ++++++++++++++++");
    //static TaskHandle_t reli_handle;
    //static uint8_t queue;
    //static uint16_t ble_conn;
    static uint8_t rx_buf[BLE_RX_BUF_SIZE];
    uint32_t len;
    test_gpio_init();
    hal_pin_set_mode(LED_RUN,HAL_PIN_MODE_OUT);
    hal_pin_write(LED_RUN,0);
    create_sample_task(); 
    suspend_reli_task();
    (void)create_scan_task();
    //scan_test();
    while(1)
    {     
        #if 0
        scan_test();
        vTaskDelay(5000);
        #else
        len = ble_slave_receive(rx_buf,sizeof(rx_buf),2000);
        if(len > 0)
        {
            DBG_LOG("Received command from ble, len=%d",len);        
            test_parse_handle(rx_buf,len,TEST_INTER_BLE);
            memset(rx_buf,0,sizeof(rx_buf));
        }
        #endif
        hal_pin_write(LED_RUN,1);
        vTaskDelay(50);
        hal_pin_write(LED_RUN,0);
        
    }
}

void test_creat_task(void)
{
    uint32_t ret = xTaskCreate( test_task_handle, "test", 512, NULL, 1, &test_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    queue_work_mode = xQueueCreate( 1, sizeof(uint8_t) );
}
void test_start(void)
{
    xSemaphoreGive(reli_sema);
}
void test_stop(void)
{
    if(reli_stop_sema == NULL)
    {
        reli_creat_stop_sema();
    }
    xSemaphoreGive(reli_stop_sema);
}

void test_set_work_mode(uint8_t type)
{
    /*
    if(queue_work_mode == NULL)
    {
        return;
    }
    xQueueSendToBack(queue_work_mode, &type, 0);
    */
   if(type == STOP)
   {
       suspend_reli_task();
   }
}



