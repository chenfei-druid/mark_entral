

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"
#include "app_error.h"
#include "ble_gap.h"

#define HAL_CRX 14
#define HAL_CSD 13
#define HAL_ANT_SEL 12
#define HAL_CPS 29
#define HAL_CTX 28
#define HAL_CHL 27

static void pa_lna_assist(uint32_t gpio_pa_pin, uint32_t gpio_lna_pin)
{
    uint32_t err_code;

    static const uint32_t gpio_toggle_ch = 0;
    static const uint32_t ppi_set_ch = 0;
    static const uint32_t ppi_clr_ch = 1;
    
    // Configure SoftDevice PA/LNA assist
    ble_opt_t opt;
    memset(&opt, 0, sizeof(ble_opt_t));
    // Common PA/LNA config
    opt.common_opt.pa_lna.gpiote_ch_id  = gpio_toggle_ch;        // GPIOTE channel
    opt.common_opt.pa_lna.ppi_ch_id_clr = ppi_clr_ch;            // PPI channel for pin clearing
    opt.common_opt.pa_lna.ppi_ch_id_set = ppi_set_ch;            // PPI channel for pin setting
    // PA config
    opt.common_opt.pa_lna.pa_cfg.active_high = 1;                // Set the pin to be active high
    opt.common_opt.pa_lna.pa_cfg.enable      = 1;                // Enable toggling
    opt.common_opt.pa_lna.pa_cfg.gpio_pin    = gpio_pa_pin;      // The GPIO pin to toggle
  
    // LNA config
    opt.common_opt.pa_lna.lna_cfg.active_high  = 1;              // Set the pin to be active high
    opt.common_opt.pa_lna.lna_cfg.enable       = 1;              // Enable toggling
    opt.common_opt.pa_lna.lna_cfg.gpio_pin     = gpio_lna_pin;   // The GPIO pin to toggle

    err_code = sd_ble_opt_set(BLE_COMMON_OPT_PA_LNA, &opt);
    //APP_ERROR_CHECK(err_code);
    sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV,0,4);
}


void pa_init(void)
{
    //NRF_LOG_INFO("%s", __FUNCTION__);

    nrf_gpio_cfg_output(HAL_CSD);
    nrf_gpio_cfg_output(HAL_ANT_SEL);
    nrf_gpio_cfg_output(HAL_CPS);
    nrf_gpio_cfg_output(HAL_CHL);

    nrf_gpio_pin_clear(HAL_ANT_SEL);
    nrf_gpio_pin_set(HAL_CSD);
    nrf_gpio_pin_clear(HAL_CPS);
    nrf_gpio_pin_set(HAL_CHL);

    pa_lna_assist(HAL_CTX,HAL_CRX);

}
