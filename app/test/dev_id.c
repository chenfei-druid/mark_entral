/**
 * @brief 
 * 
 * @file dev_id.c
 * @date 2018-08-01
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "ble_druid_svc.h"
#include "HexStr.h"
/**
 * @brief 
 * 
 * @param p_mac 
 * @param size 
 * @return true 
 * @return false 
 */

bool read_dev_id(char * const p_mac, uint32_t size)
{
    uint8_t mac_hex[7];

    uint8_t len = get_device_mac_addr(mac_hex,sizeof(mac_hex));
    if(len == 0)
    {
        return false;
    }
    HexToStr(p_mac,mac_hex,len);
    return true;
}