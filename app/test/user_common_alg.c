
/** 
 * user_common_alg.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/5/7
 * @version:     V0.0
 * 
 *
 */
#include <math.h>
#include "app_freertos.h"
#include <ctype.h>

/**
* @function@name:    user_env_get_timestamp
* @description  :    Function for getting a timestamp from rtc2
* @input        :    none
* @output       :    none
* @return       :    timestatmp
**/ 
int user_app_rank_min_to_max_32(int32_t * const a,int n)    
{    
    int i,j; 
    if(a == NULL || n == 0)
    {
        return 0;
    }
    
    for(i=0;i<n-1;i++)                
    {    
        int k=i;                    //get min position    
        for(j=i+1;j<n;j++)   
        {        
            if(a[k]>a[j])           //if ther is a smaller, get its position
                k=j;                   
        }    
        if(k!=i)                    //switch the minimum data
        {    
            int temp=a[i];    
            a[i]=a[k];    
            a[k]=temp;    
        }    
    }    
    return n;
}  
int user_app_rank_min_to_max_16(int16_t * const a,int n)    
{    
    int i,j; 
    if(a == NULL || n == 0)
    {
        return 0;
    }
    
    for(i=0;i<n-1;i++)                
    {    
        int k=i;                    //get min position    
        for(j=i+1;j<n;j++)   
        {        
            if(a[k]>a[j])           //if ther is a smaller, get its position
                k=j;                   
        }    
        if(k!=i)                    //switch the minimum data
        {    
            int temp=a[i];    
            a[i]=a[k];    
            a[k]=temp;    
        }    
    }    
    return n;
} 
//judge digit of a decimal data
int digit(int a)
{
    int r=0;
    if(a==0) return 1;
    while(a){a/=10; r++;}
    return r;
}
bool user_string_compare(char const * str1, char  * str2)
{
    while(*str1 && *str2)
    {
        if(*str1 != *str2)
        {
            return false;
        }
        str1++;
        str2++;
    }
    return true;
}
bool user_char_to_lower(char *buf)
{
    if(buf == NULL)
    {
        return false;
    }
    while(*buf)
    {
        *buf = tolower(*buf);
        buf++;
    }
    return true;
}

//end

