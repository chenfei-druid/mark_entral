
#include "test_parallel.h"
#include "FreeRTOS.h"
#include "task.h"

void parallel_reset(void)
{
    hal_pin_write(PIN_TEST,0);
    hal_pin_write(PIN_RESET,0);
    vTaskDelay(10);
    hal_pin_write(PIN_RESET,1);
}
void parellel_init(void)
{
    parallel_reset();
    hal_pin_write(PIN_TEST,0);
}
/**
 * @brief 
 * 
 * @param x_data 
 */
void parallel_write_byte(uint8_t *x_data)
{
    lvl_t *lvl = (lvl_t *)x_data;

    hal_pin_write(PIN_WR,1);

    hal_pin_write(FIFO_BUS_D0,lvl->d0);
    hal_pin_write(FIFO_BUS_D1,lvl->d1);
    hal_pin_write(FIFO_BUS_D2,lvl->d2);
    hal_pin_write(FIFO_BUS_D3,lvl->d3);
    hal_pin_write(FIFO_BUS_D4,lvl->d4);
    hal_pin_write(FIFO_BUS_D5,lvl->d5);
    hal_pin_write(FIFO_BUS_D6,lvl->d6);
    hal_pin_write(FIFO_BUS_D7,lvl->d7);

    hal_pin_write(PIN_WR,0);

}
/**
 * @brief 
 * 
 * @return uint8_t 
 */
uint8_t  parallel_read_byte(void)
{   
    lvl_t lvl;
    uint8_t *level = (uint8_t *)&lvl;
    //while(hal_pin_read(PIN_RX_ENABLE) == 1);
    hal_pin_write(PIN_RD,0);

    lvl.d0 = hal_pin_read(FIFO_BUS_D0);
    lvl.d1 = hal_pin_read(FIFO_BUS_D1);
    lvl.d2 = hal_pin_read(FIFO_BUS_D2);
    lvl.d3 = hal_pin_read(FIFO_BUS_D3);
    lvl.d4 = hal_pin_read(FIFO_BUS_D4);
    lvl.d5 = hal_pin_read(FIFO_BUS_D5);
    lvl.d6 = hal_pin_read(FIFO_BUS_D6);
    lvl.d7 = hal_pin_read(FIFO_BUS_D7);

    hal_pin_write(PIN_RD,1);
    //while(hal_pin_read(PIN_RX_ENABLE) == 0);
    ;
    ;
    return *level;
}
/**
 * @brief 
 * 
 * @param p_buf 
 * @param size 
 * @param timeout 
 * @return uint32_t 
 */
uint32_t parallel_read_data(uint8_t *p_buf, uint32_t size,uint32_t timeout)
{
    uint32_t len = 0;
    do
    {
        if(hal_pin_read(PIN_RX_ENABLE) == 1)        //the fifo data can be read when RXF pin is low
        {
            vTaskDelay(timeout);
            if(hal_pin_read(PIN_RX_ENABLE) == 1)
            {
                return len;
            }
        }
        p_buf[len++] = parallel_read_byte();

    }while(len < size);
    return len;
}
/**
 * @brief 
 * 
 * @param p_buf 
 * @param length 
 * @param timeout 
 * @return uint32_t 
 */
uint32_t parallel_write_data(uint8_t *p_buf, uint32_t length,uint32_t timeout)
{
    uint32_t len = 0;
     do
    {
        if(hal_pin_read(PIN_TX_ENABLE) == 1)        //the fifo data can be read when RXF pin is low
        {
            vTaskDelay(timeout);
            if(hal_pin_read(PIN_TX_ENABLE) == 1)
            {
                return len;
            }
        }
        parallel_write_byte(&p_buf[len++]);

    }while(len < length);
    return len;
}

