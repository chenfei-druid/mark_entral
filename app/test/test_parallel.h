#ifndef TEST_PARALLEL_H
#define TEST_PARALLEL_H

#include <stdint.h>
#include <stdbool.h>
#include "hal_pin.h"

#define FIFO_BUS_D0  0
#define FIFO_BUS_D1  1
#define FIFO_BUS_D2  2
#define FIFO_BUS_D3  3
#define FIFO_BUS_D4  4
#define FIFO_BUS_D5  5
#define FIFO_BUS_D6  6
#define FIFO_BUS_D7  7

#define PIN_RD      0
#define PIN_WR      0

#define PIN_TX_ENABLE   0
#define PIN_RX_ENABLE   0

#define PIN_RESET       0
#define PIN_TEST        0

#define TX_BUF_SIZE     128
#define RX_BUF_SIZE     256

typedef struct pin_lvl_t   lvl_t;
struct pin_lvl_t
{
    uint8_t d0:1;
    uint8_t d1:1;
    uint8_t d2:1;
    uint8_t d3:1;
    uint8_t d4:1;
    uint8_t d5:1;
    uint8_t d6:1;
    uint8_t d7:1;
};

void parellel_init(void);
uint32_t parallel_read_data(uint8_t *p_buf, uint32_t size,uint32_t timeout);
uint32_t parallel_write_data(uint8_t *p_buf, uint32_t length,uint32_t timeout);


#endif //TEST_PARALLEL_H
