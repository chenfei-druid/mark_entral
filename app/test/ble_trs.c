/**
 * @brief 
 * 
 * @file test_ble.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-06-26
 */

/*------------------------------------ Includes ----------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "config.h"
#include "ble_druid.h"

//
/*------------------------------------ user defines -----------------------------*/
static TaskHandle_t ble_task_handle = NULL;
static QueueHandle_t   test_ble_queue = NULL;
static ble_druid_t* p_druid = NULL;

/*------------------------------------ functions ----------------------------------*/
/**
 * @brief 
 * 
 * @param arg 
 */
void test_ble_task(void *arg)
{
    uint32_t evt_flag = 0;
    static bool conn_handle = false;
    uint8_t buf[100];
    uint8_t give_len = 0;
    uint8_t len_cnt = 0;
    uint8_t queue;
    xQueueReset(test_ble_queue);
    while(1)
    {        
        /*xTaskNotifyWait(pdFALSE, LONG_MAX, &evt_flag, portMAX_DELAY);        
        if(evt_flag == 0) 
        { // Disconnected
             sd_ble_gap_disconnect(0, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
             conn_handle = false;

            continue;
        } 
        else if(evt_flag == LONG_MAX) 
        { // Connected
            conn_handle = true;
        } 
        else 
        { // Data received
            ble_druid_evt_rx_data_t *p_data = (ble_druid_evt_rx_data_t *)evt_flag;
            ble_druid_send(p_druid,p_data->p_data,p_data->length,conn_handle);
            continue;
        }
        */
        give_len = 0;
        len_cnt = 0;
        if(xQueueReceive(test_ble_queue, &queue , portMAX_DELAY) != pdPASS)
        {
            continue;
        }
        
        if(queue > 0)
        {
            give_len = queue;
            while(xQueueReceive(test_ble_queue, &queue , portMAX_DELAY) != errQUEUE_EMPTY)
            {
                buf[len_cnt++] = queue;
                if(len_cnt >= sizeof(buf))
                {
                    break;
                }
            }
            if(give_len != len_cnt)
            {
                continue;
            }
            ble_druid_send(p_druid,buf,len_cnt,conn_handle);
        }
        xQueueReset(test_ble_queue);
        uint8_t temp = 0x13;
        //ble_druid_send(p_druid,&temp,1,0);
    }
}
/**
 * @brief 
 * 
 * @param arg 
 * @return TaskHandle_t 
 */
TaskHandle_t test_create_ble_task(void* arg) 
{
    ble_task_handle = NULL;

    p_druid = (ble_druid_t*) arg;
    xTaskCreate(test_ble_task, "test", 256, arg, 1, &ble_task_handle);
    test_ble_queue = xQueueCreate( 100, sizeof(uint8_t) );

    return ble_task_handle;
}

bool test_trans_queue(uint8_t *buf, uint8_t len)
{
    if(buf ==NULL || len == 0)
    {
        return false;
    }
    while(xQueueSendToBack(test_ble_queue, &len, 10) != pdPASS);
    for(uint8_t i=0;i<len;i++)
    {
        while(xQueueSendToBack(test_ble_queue, &buf[i], 10) != pdPASS);
    }
}
