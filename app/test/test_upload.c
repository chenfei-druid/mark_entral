/**
 * @brief 
 * 
 * @file test_upload.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-11
 */
#include "test_upload.h"
#include "test_uart.h"
#include "user_app_log.h"
#include "ble_trans.h"

bool test_upload_data(void const *p_buf, uint8_t len, test_inter_type type)
{
    if(p_buf ==NULL || len == 0 || type >= TEST_INTER_MAX)
    {
        return false;
    }
    uint8_t *buf = (uint8_t *)p_buf;
    if(type == TEST_INTER_BLE)
    {
        //BT_Send(0, buf,len);
        ble_s_pkg_send((uint8_t *)buf,len); 
        DBG_LOG("ble send: %s",buf);
    }
    else if(type == TEST_INTER_UART)
    {
        //send through uart interface
        bool ret = test_uart_trans(buf,len);
        if(ret == 0)
        {
            DBG_LOG("uart send failed",buf);
            return false;
        }
        for(uint8_t i=0;i<len;i++)
        {
            DBG_LOG("uart send: 0x%x",buf[i]);
        }
    }
    else if(type == TEST_INTER_PARALLEL)
    {
        //test_parallel_trans(buf,len);
        DBG_LOG("ble send: %s",buf);
    }
    return false;
}
