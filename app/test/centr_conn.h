/**
 * @brief 
 * 
 * @file test_centr_cmd_conn.h
 * @date 2018-08-02
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TEST_BLE_CONN_H
#define TEST_BLE_CONN_H

#include <stdint.h>
#include "test_upload.h"


#define CENTR_CONN_VERIFY 0


#if CENTR_CONN_VERIFY
void pb_verify(void);
void connect_test(void);
#endif //VERIFY

bool centr_ble_conn(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);
bool connect_slave(void);
void res_fail(test_inter_type chn);

#endif  //TEST_BLE_CONN_H
