
/** 
 * user_app_identity.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/25
 * @version:     V0.0
 *
 */
 #include <ctype.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "ble_druid_svc.h"
#include "user_data_pkg.h"

#include "user_app_identity.h"
#include "crc16.h"
#include "app_error.h"
#include "HexStr.h"
#include "user_app_log.h"
#include "user_common_alg.h"


/*********************************************user defines*****************************************************/
//static const char device_id[13];
/*********************************************user functions*****************************************************/

/**
* @function@name:    app_iden_get_device_id
* @description  :    function for get mac address and convert into char
* @input        :    p_id:pointer of id buffer
* @output       :    none
* @return       :    true if success
*/
bool app_iden_get_device_id_char(char *p_id)
{
    if(p_id == NULL)
    {
        return false;
    }
    uint8_t mac_addr[DEVICE_MAC_ADDR_LEN];
    get_device_mac_addr(mac_addr,sizeof(mac_addr));
    
    HexToStr(p_id,mac_addr,DEVICE_MAC_ADDR_LEN);
    user_char_to_lower(p_id);
    return true;
}
bool app_add_colon_to_mac_addr(char *const p_mac)
{
    char mac[18];
    if(p_mac == NULL)
    {
        return 0;
    }
    if(strlen(p_mac) > 12)
    {
        return 0;
    }
    for(uint8_t i=0;i<strlen(p_mac);i++)
    {
        p_mac[i] = tolower(p_mac[i]);
    }
    strcpy(mac,p_mac);
    p_mac[0] = mac[0];
    p_mac[1] = mac[1];
    p_mac[2] = ':';
    p_mac[3] = mac[2];
    p_mac[4] = mac[3];
    p_mac[5] = ':';
    p_mac[6] = mac[4];
    p_mac[7] = mac[5];
    p_mac[8] = ':';
    p_mac[9] = mac[6];
    p_mac[10] = mac[7];
    p_mac[11] = ':';
    p_mac[12] = mac[8];
    p_mac[13] = mac[9];
    p_mac[14] = ':';
    p_mac[15] = mac[10];
    p_mac[16] = mac[11];
    p_mac[17] = 0;
    return true;
}
/**
* @function@name:    app_get_msg_token
* @description  :    function for get message token, take device id(mac) and a random value to compute crc16, 
*               :    then combine ((left shift the result of 16-bit) (or) random value);
* @input        :    dev_id:pointer of dev_id char buffer
* @output       :    none
* @return       :    return result of msgtoken
*/
uint32_t app_get_msg_token(char const * const dev_id, uint16_t const random)
{
    if(dev_id == NULL)
    {
        return 0;
    }
    uint16_t ran = rand() & 0xffff;
    ran = random==0? (rand() & 0xffff):random;
    //ran = 0x1234;
    uint32_t msgtoken = 0;
    
    uint8_t token_temp[DEVICE_MAC_ADDR_CHAR_LEN+2] = {0};
    uint16_t crc16 = 0;
        
    memcpy(token_temp,dev_id,DEVICE_MAC_ADDR_CHAR_LEN);
    memcpy(&token_temp[DEVICE_MAC_ADDR_CHAR_LEN],(const char *)&ran,sizeof(uint16_t));
    
    crc16 = crc16_compute(token_temp,DEVICE_MAC_ADDR_CHAR_LEN + sizeof(uint16_t), NULL);

    msgtoken = crc16 << 16 | ran;
    
    DBG_LOG("\r\nrandom = %x, msgtoken = %x\r\n",ran,msgtoken);
    return msgtoken;
}
/**
* @function@name:    app_get_indetity_msg
* @description  :    function for getting identity message
* @input        :    dev_id:pointer of dev_id char buffer
* @output       :    none
* @return       :    return result of msgtoken
*/
bool app_get_indetity_msg(protocol_identity_msg_t * p_iden_msg)
{
    if(p_iden_msg == NULL )
    {
        return false;
    }
    char dev_id_arg[DEVICE_MAC_ADDR_CHAR_LEN+1];
    
    app_iden_get_device_id_char(dev_id_arg);
    DBG_LOG("\r\n~~~~~~~~~~~~~~~~~dev_id_arg = %s~~~~~~~~~~~~~~~~~~~~~~~\r\n",dev_id_arg);
    p_iden_msg->MsgToken = app_get_msg_token(dev_id_arg,0);
    //p_iden_msg->MsgToken = app_get_msg_token(device_id);
    p_iden_msg->has_RspCode = false;
    p_iden_msg->MsgIndex++;   
    
    //p_iden_msg->DeviceID.arg = dev_id_arg;
    return true;
}
/*
bool user_init_idendity_device_id(void)
{
    app_iden_get_device_id_to_char((char *)device_id);
    DBG_LOG("\r\ninitialized dev_id_arg = %s~~~~~~~~~~~~~~~~~~~~~~~\r\n",device_id);
}
bool app_iden_get_device_id_char(char *p_id)
{
    strcpy(p_id,device_id);
}
*/
//end functions

