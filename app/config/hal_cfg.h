/*!
 *    @file  hal_cfg.h
 *   @brief  The config file for hal layer
 *
 *  @author  Dale.J (dj), Dale.J@zoho.com
 *
 *  @internal
 *       Created:  05/17/2018
 *      Revision:  none
 *  Organization:  Druid Tech
 *     Copyright:  Copyright (c) 2016, Dale.J
 *
 *  This source code is released for free distribution under the terms of the
 *  GNU General Public License as published by the Free Software Foundation.
 */

#ifndef __HAL_CFG_H__
#define __HAL_CFG_H__

/* Defines -------------------------------------------------------------------*/
#define SW_VERSION        1

// FLASH
#define HAL_CFG_FLS_NSS   30
#define HAL_CFG_FLS_MISO  29
#define HAL_CFG_FLS_MOSI  28
#define HAL_CFG_FLS_SCK   27

// KX022
#define HAL_CFG_ACC_INT   12
#define HAL_CFG_ACC_NSS   11
#define HAL_CFG_ACC_SCK   10
#define HAL_CFG_ACC_MISO  9
#define HAL_CFG_ACC_MOSI  8

// I2C
#define HAL_CFG_I2C_SCL   26
#define HAL_CFG_I2C_SDA   25

// HALL
#define HAL_CFG_HALL_IRQ  20

// GPS
#define HAL_CFG_GPS_PWR    31
#define HAL_CFG_GPS_BKP    5
#define HAL_CFG_GPS_RXD    6
#define HAL_CFG_GPS_TXD    7

// BATTERY
#define HAL_CFG_BATTERY    4

// LED
#define HAL_CFG_LED_GREEN  21

// CELL
#define HAL_CFG_CELL_PWR   13
#define HAL_CFG_CELL_RI    14
#define HAL_CFG_CELL_DTR   15
#define HAL_CFG_CELL_TXD   16
#define HAL_CFG_CELL_RXD   17
#define HAL_CFG_CELL_STA   18
#define HAL_CFG_CELL_KEY   19

#endif // #ifndef __HAL_CFG_H__
