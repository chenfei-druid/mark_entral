/**
 * @brief 
 * 
 */
#include <stdlib.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
//#include "semphr.h"
//#include "queue.h"
#include "ble_trans.h"
#include "user_data_pkg.h"
#include "ble_pkg.h"
#include "user_app_log.h"
#include "ble_druid_svc.h"

#define BLE_TX_MAX_BUF  800

static QueueHandle_t        ble_central_queue = NULL;
static QueueHandle_t        ble_slave_rx_queue = NULL;
//static SemaphoreHandle_t    ble_central_sema = NULL;
static SemaphoreHandle_t    sema_c_tx_complete = NULL;
static SemaphoreHandle_t    sema_s_tx_complete = NULL;

QueueHandle_t ble_c_create_queue(void)
{
    ble_central_queue = xQueueCreate(2, sizeof(uint32_t) );
    xQueueReset(ble_central_queue);
    return ble_central_queue;
}
QueueHandle_t ble_s_create_queue(void)
{
    ble_slave_rx_queue = xQueueCreate(2, sizeof(uint32_t) );
    xQueueReset(ble_slave_rx_queue);
    return ble_slave_rx_queue;
}
SemaphoreHandle_t ble_c_create_sema(void)
{
  sema_c_tx_complete = xSemaphoreCreateBinary();
  return sema_c_tx_complete;
}
SemaphoreHandle_t ble_s_create_sema(void)
{
    sema_s_tx_complete = xSemaphoreCreateBinary();
    return sema_s_tx_complete;
}
#if 0
uint32_t ble_c_receive(void * const buffer, uint16_t max_size, uint32_t timeout)
{   
    if(buffer == NULL || max_size == 0)
    {
        DBG_LOG("ble c receive pointer is null");
        return 0;
    }
    //uint32_t queue = 0;
    //uint8_t *p_buf = (uint8_t *)queue;
    uint8_t * p_buf = NULL;
    uint32_t rx_len = 0;
    if(ble_central_queue == NULL)
    {
        return 0;
    }
    //DBG_LOG("ble_max_len = %d",ble_max_len);
    if(xQueueReceive(ble_central_queue, &rx_len , timeout) != pdPASS)
    {
        //DBG_LOG("ble receive timeout");
        return 0;
    } 
    if(rx_len > 0 && rx_len <= max_size)
    {
        DBG_LOG("ble rx len = %d",rx_len);
        //p_buf = (uint8_t *)malloc(give_len * sizeof(uint8_t));
        xQueueReceive(ble_central_queue, &p_buf , 0);
        DBG_LOG("rec address = 0x%x",p_buf);
        memcpy(buffer,p_buf,rx_len);
    }
    else
    {
        if(rx_len > max_size)
        {
            DBG_LOG("rx_len = %d > max size = %d",rx_len,max_size);
        }
        else
        {
            DBG_LOG("rx_len = 0");
        }
        return 0;
    } 
}
#else
uint32_t ble_c_receive(void * const buffer, uint16_t max_size, uint32_t timeout)
{   
    if(buffer == NULL || max_size == 0)
    {
        DBG_LOG("ble c receive pointer is null");
        return 0;
    }
    //uint32_t queue = 0;
    //uint8_t *p_buf = (uint8_t *)queue;
    uint8_t * p_buf = NULL;
    uint32_t rx_len = 0;
    if(ble_central_queue == NULL)
    {
        return 0;
    }
    //DBG_LOG("ble_max_len = %d",ble_max_len);
    if(xQueueReceive(ble_central_queue, &rx_len , timeout) != pdPASS)
    {
        //DBG_LOG("ble receive timeout");
        return 0;
    } 
    DBG_LOG("ble rx len = %d",rx_len);
    if(rx_len > 0 && rx_len <= max_size)
    {
        //p_buf = (uint8_t *)malloc(give_len * sizeof(uint8_t));
        xQueueReceive(ble_central_queue, &p_buf , 0);
        //DBG_LOG("rec address = 0x%x",p_buf);
        rx_len = ble_get_valid_field(buffer,p_buf,rx_len);        
        return rx_len;
    }
    else
    {
        if(rx_len > max_size)
        {
            DBG_LOG("rx_len = %d > max size = %d",rx_len,max_size);
        }
        else
        {
            DBG_LOG("rx_len = 0");
        }
        return 0;
    } 
}
#endif

uint32_t ble_c_send(void * const buffer, uint16_t length)
{

    if(length <= 0 || buffer == NULL)
    {
        return false;
    }
    uint8_t     max_mtu     = BLE_MAX_LEN;//get_ble_max_data_len();
    uint16_t    len_cnt     = length;   
    uint32_t    err_code    = NRF_SUCCESS;      
    uint8_t     *send_buff  ;   
    uint8_t     sub_pkg_len = 0;
    uint8_t    *p_data      = (uint8_t *)buffer;

    send_buff = pvPortMalloc(max_mtu);
    if(send_buff == NULL)
    {
        return 0;
    }
    if(sema_c_tx_complete == NULL)
    {
        return 0;
    }
    xSemaphoreTake( sema_c_tx_complete, 0 );
    DBG_LOG("ble send beginning...\r\nthe max send len = %d\r\n, total len = %d\r\n", max_mtu,length);  
    do
    {            
        //split the datas to sub package  to send
        user_get_sub_pkg_from_cache(&p_data[length-len_cnt],send_buff,len_cnt,length,max_mtu-1);
        if(len_cnt < max_mtu)
        {            
            sub_pkg_len = len_cnt;
            err_code = ble_act_central_send(send_buff,len_cnt+1);
            len_cnt = 0;
        }
        else    //len_cnt > m_ble_rts_max_data_len
        {
            sub_pkg_len = max_mtu - 1;
            err_code = ble_act_central_send(send_buff,max_mtu);
            len_cnt -= (max_mtu - 1);
        }
        if(err_code != NRF_SUCCESS)
        {
            if(err_code == NRF_ERROR_INVALID_STATE)
            {
               //DBG_LOG("ble transmit failed: NRF_ERROR_INVALID_STATE\r\n");
                return false;
            }
            if(err_code == NRF_ERROR_INVALID_PARAM)
            {
                //DBG_LOG("ble transmit failed with NRF_ERROR_INVALID_PARAM\r\n");
                return false;
            }
            //ble send buffer is full, waiting for tx complete
            len_cnt += sub_pkg_len;            
            if(xSemaphoreTake( sema_c_tx_complete, BLE_TX_COMPLETE_WAIT ) != pdPASS)
            {
                DBG_LOG(">>>>>>>>>>>>>>  ble transmit timeout  <<<<<<<<<<<<<< %d\r\n");
                return false;
            }
            //DBG_LOG("received BLE_GATTS_EVT_HVN_TX_COMPLETE\r\n");
        }
        DBG_LOG("\r\nble current split pkg len = %d, remain len = %d\r\n",sub_pkg_len,len_cnt);
    }while(len_cnt);      
    DBG_LOG("ble send data complete\r\n");
    return true;
}
bool ble_c_pkg_and_send(void * const buffer, uint16_t length)
{
    if(buffer == NULL)
    {
        return false;
    }   
    uint8_t *tx_buf = NULL;
    uint32_t len = 0;

    vPortFree(tx_buf);
    tx_buf = pvPortMalloc(BLE_TX_MAX_BUF);

    len = ble_package(tx_buf,buffer,BLE_TX_MAX_BUF,length);

    if(len > 0)
    {
        len = ble_c_send(tx_buf,len);
    }
    vPortFree(tx_buf);
    if(len)
    {
        return true;
    }
    return false;
}

uint32_t ble_slave_receive(void * const buffer, uint16_t const max_size, uint32_t timeout)
{
    if(buffer == NULL || max_size == 0)
    {
        DBG_LOG("ble c receive pointer is null");
        return 0;
    }
    uint8_t * p_buf = NULL;
    uint32_t rx_len = 0;
    if(ble_slave_rx_queue == NULL)
    {
        vTaskDelay(timeout);
        return 0;
    }
    if(xQueueReceive(ble_slave_rx_queue, &rx_len , timeout) != pdPASS)
    {
        //DBG_LOG("ble receive timeout");
        return 0;
    } 
    DBG_LOG("ble slave rx len = %d",rx_len);
    if(rx_len > 0 && rx_len <= max_size)
    {
        xQueueReceive(ble_slave_rx_queue, &p_buf , 0);
        DBG_LOG("rec address = 0x%x",p_buf);
        rx_len = ble_get_valid_field(buffer,p_buf,rx_len);       
        return rx_len;
    }
    else
    {
        if(rx_len > max_size)
        {
            DBG_LOG("rx_len = %d > max size = %d",rx_len,max_size);
        }
        else
        {
            DBG_LOG("rx_len = 0");
        }
        return 0;
    } 
}
static int ble_s_send_one_split(uint8_t * const p_buf, uint16_t length)
{
    uint8_t *buf = (uint8_t *)p_buf;
    if(buf == NULL)
    {
        return false;
    }
    if(length <= 0 || length > get_ble_s_max_data_len())
    {
        return false;
    } 
    //DBG_LOG("%s \r\n",p_buf);
#if 1
#include "HexStr.h"
    char str[256] = {0};
    HexToStr(str,(uint8_t *)buf,length);
    DBG_LOG("%s \r\n",str);
    SEGGER_RTT_printf(0,"%s ",str);
    SEGGER_RTT_printf(0,"\r\n\r\n");
#endif
    uint32_t err_code = ble_act_slave_send((uint8_t*)buf,length);
    
    if(err_code != NRF_SUCCESS)
    {
        if(err_code == NRF_ERROR_NOT_FOUND)
        {
            DBG_LOG("NRF_ERROR_NOT_FOUND\r\n");
            return -1;
        }
        if(err_code == NRF_ERROR_INVALID_STATE)
        {
            DBG_LOG("ble transmit failed: NRF_ERROR_INVALID_STATE\r\n");
            return -1;
        }
        if(err_code == NRF_ERROR_INVALID_PARAM)
        {
            DBG_LOG("ble transmit failed with NRF_ERROR_INVALID_PARAM\r\n");
            return -1;
        }
        //ble send buffer is full, waiting for tx complete            
        if(xSemaphoreTake( sema_s_tx_complete, BLE_TX_COMPLETE_WAIT ) == pdPASS)
        {
            DBG_LOG("received sema of BLE_GATTS_EVT_HVN_TX_COMPLETE\r\n");
            return -2;
        }
        DBG_LOG("-----  ble transmit timeout  %dS -----\r\n",BLE_TX_COMPLETE_WAIT);
        return -1;
    }
#if 0
    DBG_LOG("send one split");
    for(uint8_t i=0;i<length;i++)
    {
        DBG_D("%x ",buf[i]);
    }
    DBG_D("\r\n");
#endif
    return 0;
}
/**
 * @brief 
 * 
 * @param p_buf 
 * @param length 
 * @return uint32_t 
 */
uint32_t ble_s_send(void * const buffer, uint16_t const length)
{
    if(length <= 0 || buffer == NULL)
    {
        return false;
    }
    uint8_t     max_mtu     = get_ble_s_max_data_len();//get_ble_max_data_len();
    uint16_t    len_cnt     = length;   
    //uint32_t    err_code    = NRF_SUCCESS;      
    uint8_t     send_buff[NRF_SDH_BLE_GATT_MAX_MTU_SIZE] ;   
    uint8_t     sub_pkg_len = 0;
    uint8_t    *p_data      = (uint8_t *)buffer;
    int ret = 0;
    //uint8_t count = 0;

    //vPortFree(send_buff);
    //send_buff = pvPortMalloc(max_mtu);

    if(sema_s_tx_complete == NULL)
    {
        sema_s_tx_complete = xSemaphoreCreateBinary();
    }
    xSemaphoreTake( sema_s_tx_complete, 0 );  
    do
    {            
        //split the datas to sub package  to send
        sub_pkg_len = user_get_sub_pkg_from_cache(&p_data[length-len_cnt],send_buff,len_cnt,length,max_mtu-1);
        ret = ble_s_send_one_split(send_buff,(sub_pkg_len+1));
        if(ret == 0)
        {
            DBG_LOG("send success length = %d",sub_pkg_len);
            if(len_cnt < max_mtu)
            {            
                len_cnt = 0;
            }
            else    
            {
                len_cnt -= sub_pkg_len;
            }
        }
        else if(ret == -2)
        {
            //len_cnt = len_cnt;
            DBG_LOG("send failed length = %d",sub_pkg_len);
        }
        else
        {
            //re-send here
            DBG_LOG("send failed length = %d",sub_pkg_len);
            break;
        }
        DBG_LOG("remanin package len = %d\r\n",len_cnt);
    }while(len_cnt);    
    vSemaphoreDelete( sema_s_tx_complete );
    sema_s_tx_complete = NULL;
    DBG_LOG("ble send data complete\r\n");
    return (ret == 0)? true:false;
}
/**
 * @brief 
 * 
 * @param buffer 
 * @param length 
 * @return true 
 * @return false 
 */
uint32_t ble_s_pkg_send(void * const buffer, uint16_t length)
{
    if(buffer == NULL)
    {
        return false;
    }   
    uint8_t *tx_buf = NULL;
    uint32_t len = 0;
    if(get_connect_status() != true)
    {
        return 0;
    }
    vPortFree(tx_buf);
    tx_buf = pvPortMalloc(BLE_TX_MAX_BUF);
    if(tx_buf == NULL)
    {
        DBG_LOG("malloc failed");
        return 0;
    }
    len = ble_package(tx_buf,(uint8_t *)buffer,BLE_TX_MAX_BUF,length);
    DBG_LOG("ble packaged total len = %d\r\n",len);
    if(len > 0)
    {        
        len = ble_s_send(tx_buf,len);
    }
    vPortFree(tx_buf);
    return len;
}

