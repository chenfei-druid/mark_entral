/**
 * @brief 
 * 
 * @file ble_druid_svc.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-10
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "peer_manager.h"
#include "app_timer.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_db_discovery.h"
#include "ble_nus.h"
#include "ble_nus_c.h"
//#include "ble_rscs.h"
//#include "ble_hrs_c.h"
//#include "ble_rscs_c.h"
#include "ble_conn_state.h"
#include "nrf_fstorage.h"
#include "fds.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"

#include "nrf_sdh_freertos.h"
#include "nrf_sdh_freertos.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "user_app_log.h"
#include "ble_trans.h"
#include "ble_pkg.h"
#include "ble_druid_svc.h"
#include "test_reliability.h"
#include "test_task.h"
#include "scan.h"
#include "ble_dfu.h"

#define USER_DUF_ENABLE                 1

#define PERIPHERAL_ADVERTISING_LED      BSP_BOARD_LED_2
#define PERIPHERAL_CONNECTED_LED        BSP_BOARD_LED_3
#define CENTRAL_SCANNING_LED            BSP_BOARD_LED_0
#define CENTRAL_CONNECTED_LED           BSP_BOARD_LED_1

#define DEVICE_NAME                     "DRUID_MRK_C"                                     /**< Name of device used for advertising. */
#define MANUFACTURER_NAME               "DRUID"                       /**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                300                                         /**< The advertising interval (in units of 0.625 ms). This value corresponds to 187.5 ms. */

#define APP_ADV_DURATION                18000                                       /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                  1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                           /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                           /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size in octets. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size in octets. */

#define SCAN_INTERVAL                   100                                      /**< Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW                     1200                                      /**< Determines scan window in units of 0.625 millisecond. */

#define SCAN_DURATION                   0x0000                                      /**< Duration of the scanning in units of 10 milliseconds. If set to 0x0000, scanning will continue until it is explicitly disabled. */


#define MIN_CONNECTION_INTERVAL         (uint16_t) MSEC_TO_UNITS(15, UNIT_1_25_MS) /**< Determines minimum connection interval in milliseconds. */
#define MAX_CONNECTION_INTERVAL         (uint16_t) MSEC_TO_UNITS(50, UNIT_1_25_MS)  /**< Determines maximum connection interval in milliseconds. */
#define SLAVE_LATENCY                   0                                           /**< Determines slave latency in terms of connection events. */
#define SUPERVISION_TIMEOUT             (uint16_t) MSEC_TO_UNITS(4000, UNIT_10_MS)  /**< Determines supervision time-out in units of 10 milliseconds. */

/**@brief   Priority of the application BLE event handler.
 * @note    You shouldn't need to modify this value.
 */
#define APP_BLE_OBSERVER_PRIO           3
#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

static ble_nus_c_t m_nus_c;
//static nrf_ble_qwr_t m_qwr;
//BLE_NUS_C_DEF(m_nus_c);
BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);                   /**< BLE NUS service instance for peripheral. */
NRF_BLE_GATT_DEF(m_gatt);                                           /**< GATT module instance. */
NRF_BLE_QWRS_DEF(m_qwr, NRF_SDH_BLE_TOTAL_LINK_COUNT);              /**< Context for the Queued Write module.*/
//NRF_BLE_QWR_DEF(m_qwr);
BLE_ADVERTISING_DEF(m_advertising);                                 /**< Advertising module instance. */
//BLE_DB_DISCOVERY_ARRAY_DEF(m_db_discovery, 2);                      /**< Database discovery module instances. */
BLE_DB_DISCOVERY_DEF(m_db_discovery);                                        /**< DB discovery module instance. */

static uint16_t m_conn_handle_c  = BLE_CONN_HANDLE_INVALID;
static uint16_t m_conn_handle_s         = BLE_CONN_HANDLE_INVALID;
/**@brief names which the central applications will scan for, and which will be advertised by the peripherals.
 *  if these are set to empty strings, the UUIDs defined below will be used
 */
static char m_target_periph_name[32] = "DEBUT-MRK\0";
//static uint8_t slave_mac[] = {0xFE, 0x80, 0x27, 0xCC, 0x7D, 0xE5};
//static uint8_t slave_mac[] = {0xE3, 0x98, 0x21, 0x05, 0x71, 0x4E};
static uint8_t slave_mac[] = {0x4E, 0x71, 0x05, 0x21, 0x98, 0xE3};
static uint8_t ble_s_max_len = 20;
static SemaphoreHandle_t    sema_c_s_connect = NULL;
static SemaphoreHandle_t    sema_scan = NULL;
static QueueHandle_t queue_slv_rx = NULL ;
static uint8_t * slv_rx_buf = NULL;
/**@brief UUIDs which the central applications will scan for if the name above is set to an empty string,
 * and which will be advertised by the peripherals.
 */
static ble_uuid_t m_adv_uuids[] =
{
    {BLE_UUID_NUS_S_SERVICE,        BLE_UUID_TYPE_BLE},
};

/**@brief Parameters used when scanning. */
static ble_gap_scan_params_t const m_scan_params =
{
    .extended      = 0,
    .active        = 1,
    .interval      = SCAN_INTERVAL,
    .window        = SCAN_WINDOW,
    .timeout       = SCAN_DURATION,
    .scan_phys     = BLE_GAP_PHY_1MBPS,
    .filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL,
};

static uint8_t m_scan_buffer_data[BLE_GAP_SCAN_BUFFER_EXTENDED_MIN]; /**< buffer where advertising reports will be stored by the SoftDevice. */

/**@brief Pointer to the buffer where advertising reports will be stored by the SoftDevice. */
static ble_data_t m_scan_buffer =
{
    m_scan_buffer_data,
    BLE_GAP_SCAN_BUFFER_EXTENDED_MIN
};

/**@brief Connection parameters requested for connection. */
static ble_gap_conn_params_t const m_connection_param =
{
    MIN_CONNECTION_INTERVAL,
    MAX_CONNECTION_INTERVAL,
    SLAVE_LATENCY,
    SUPERVISION_TIMEOUT
};

uint8_t get_ble_s_max_data_len(void)
{
    return ble_s_max_len;
}
/**@brief Function to handle asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing ASSERT call.
 * @param[in] p_file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}

/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

bool scan_start_name(const char * const p_name)
{
    if(p_name != NULL)
    {
        memset(m_target_periph_name,0,sizeof(m_target_periph_name));
        //strcpy(m_target_periph_name,p_name);
        memcpy(m_target_periph_name,p_name,strlen(p_name));
    }
    ret_code_t err_code;
    (void) sd_ble_gap_scan_stop();
    err_code = sd_ble_gap_scan_start(&m_scan_params, &m_scan_buffer);
    if(err_code != NRF_SUCCESS)
    {
        DBG_LOG("scan start request failed");
        return false;
    }
    return true;
}
/**@brief Function for initiating scanning.
 */
void scan_start(void)
{
    ret_code_t err_code;

    (void) sd_ble_gap_scan_stop();

    err_code = sd_ble_gap_scan_start(&m_scan_params, &m_scan_buffer);
    // It is okay to ignore this error since we are stopping the scan anyway.
    if (err_code != NRF_ERROR_INVALID_STATE)
    {
        APP_ERROR_CHECK(err_code);
    }
}
void scan_stop(void)
{
    (void)sd_ble_gap_scan_stop();
}
/**@brief Function for initiating advertising and scanning.
 */
static void adv_scan_start(void)
{
    //ret_code_t err_code;

    //check if there are no flash operations in progress
    if (!nrf_fstorage_is_busy(NULL))
    {
        // Start scanning for peripherals and initiate connection to devices which
        // advertise Heart Rate or Running speed and cadence UUIDs.
        scan_start();

        // Turn on the LED to signal scanning.
        //bsp_board_led_on(CENTRAL_SCANNING_LED);

        // Start advertising.
        //err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
        //APP_ERROR_CHECK(err_code);
    }
}
bool get_connect_status(void)
{
    if(m_conn_handle_s != BLE_CONN_HANDLE_INVALID)
    {
        return true;
    }
    return false;
}
/**
 * @brief Get the device mac addr object
 * 
 * @param p_mac 
 */
uint8_t get_device_mac_addr(uint8_t * const p_mac, uint8_t const size)
{
    uint32_t err_code;
    ble_gap_addr_t gap_addr;
    if((p_mac == NULL) || (size < 6))
    {
        return 0;
    }
    err_code = sd_ble_gap_addr_get(&gap_addr);
    if(err_code != NRF_SUCCESS)
    {
        return 0;
    }

    for(uint8_t i=0;i<6;i++)
    {
        p_mac[i] = gap_addr.addr[5-i];
    }
    return 6;
}
/**
 * @brief 
 * 
 * @param p_mac 
 * @param timeout 
 * @return true 
 * @return false 
 */
bool ble_connect_slave(uint16_t *const conn,const uint8_t * const p_mac,uint32_t timeout)
{
    //DBG_LOG("Connecting to lsave.");
    
    uint32_t err_code;
    ble_gap_addr_t ble_gap_addr;
    memset(&ble_gap_addr,0,sizeof(ble_gap_addr));
    
    if(p_mac != NULL)
    {
        memcpy(ble_gap_addr.addr,p_mac,BLE_GAP_ADDR_LEN);
    }
    else
    {
        memcpy(ble_gap_addr.addr,slave_mac,BLE_GAP_ADDR_LEN);
    }
    ble_gap_addr.addr_id_peer = 0;
    ble_gap_addr.addr_type = BLE_GAP_ADDR_TYPE_RANDOM_STATIC;
    err_code = sd_ble_gap_connect(&ble_gap_addr,
                                    &m_scan_params,
                                    &m_connection_param,
                                    APP_BLE_CONN_CFG_TAG);        
    if (err_code != NRF_SUCCESS)
    {
        DBG_LOG("Connection Request Failed, reason %d", err_code);
        return false;
    }

    //scan_start();
    bool ret = false;
    if(sema_c_s_connect == NULL)
    {
        sema_c_s_connect = xSemaphoreCreateBinary();
        xSemaphoreTake(sema_c_s_connect,0);
    }
    DBG_LOG("Waiting to connect slave device.");
    if(xSemaphoreTake(sema_c_s_connect,timeout) == pdPASS)
    {
        if((m_conn_handle_c != BLE_CONN_HANDLE_INVALID) &&
          (m_conn_handle_s != BLE_CONN_HANDLE_INVALID))
        {
            ret = true;
        }
    }
    else
    {
        DBG_LOG("Connection timeout");
    }
    //scan_stop();
    DBG_LOG("Connect slave %s.",ret?"successed":"failed");
    vSemaphoreDelete(sema_c_s_connect);
    sema_c_s_connect = NULL;
    
    return ret;
    
}
bool ble_disconnect_slave(uint16_t conn)
{
    uint32_t err_code;
    if(m_nus_c.conn_handle == BLE_CONN_HANDLE_INVALID)
    {
        return false;
    }
    err_code = sd_ble_gap_disconnect(m_nus_c.conn_handle,
                                    BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    APP_ERROR_CHECK(err_code);
    DBG_LOG("disconnect slave.");
    return true;
}
bool ble_scan_slave(uint32_t timeout)
{
    adv_scan_start();
    DBG_LOG("scan starting.");
    if(sema_scan == NULL)
    {
        sema_scan = xSemaphoreCreateBinary();
        xSemaphoreTake(sema_scan,0);
    }
    if(xSemaphoreTake(sema_scan,timeout) == pdPASS)
    {
        vSemaphoreDelete(sema_scan);
        return true;
    }
    return false;
}

#if USER_DUF_ENABLE
// YOUR_JOB: Update this code if you want to do anything given a DFU event (optional).
/**@brief Function for handling dfu events from the Buttonless Secure DFU service
 *
 * @param[in]   event   Event from the Buttonless Secure DFU service.
 */
static void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
            NRF_LOG_INFO("Device is preparing to enter bootloader mode.");
            // YOUR_JOB: Disconnect all bonded devices that currently are connected.
            //           This is required to receive a service changed indication
            //           on bootup after a successful (or aborted) Device Firmware Update.
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            // YOUR_JOB: Write app-specific unwritten data to FLASH, control finalization of this
            //           by delaying reset by reporting false in app_shutdown_handler
            NRF_LOG_INFO("Device will enter bootloader mode.");
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("Request to enter bootloader mode failed asynchroneously.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("Request to send a response to client failed.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            APP_ERROR_CHECK(false);
            break;

        default:
            NRF_LOG_ERROR("Unknown event from ble_dfu_buttonless.");
            break;
    }
}
#endif //USER_DUF_ENABLE
/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    ret_code_t err_code;

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            DBG_LOG("Connected to a previously bonded device.");
        } break;

        case PM_EVT_CONN_SEC_SUCCEEDED:
        {
            DBG_LOG("Connection secured: role: %d, conn_handle: 0x%x, procedure: %d.",
                         ble_conn_state_role(p_evt->conn_handle),
                         p_evt->conn_handle,
                         p_evt->params.conn_sec_succeeded.procedure);
        } break;

        case PM_EVT_CONN_SEC_FAILED:
        {
            /* Often, when securing fails, it shouldn't be restarted, for security reasons.
             * Other times, it can be restarted directly.
             * Sometimes it can be restarted, but only after changing some Security Parameters.
             * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
             * Sometimes it is impossible, to secure the link, or the peer device does not support it.
             * How to handle this error is highly application dependent. */
        } break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
        {
            // Reject pairing request from an already bonded peer.
            pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } break;

        case PM_EVT_STORAGE_FULL:
        {
            // Run garbage collection on the flash.
            err_code = fds_gc();
            if (err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                APP_ERROR_CHECK(err_code);
            }
        } break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
        {
            //adv_scan_start();
            DBG_LOG("**scan start at PM_EVT_PEERS_DELETE_SUCCEEDED**.");
        } break;

        case PM_EVT_PEER_DATA_UPDATE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
        } break;

        case PM_EVT_PEER_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
        } break;

        case PM_EVT_PEERS_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
        } break;

        case PM_EVT_ERROR_UNEXPECTED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
        } break;

        case PM_EVT_CONN_SEC_START:
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
        case PM_EVT_PEER_DELETE_SUCCEEDED:
        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED:
            // This can happen when the local DB has changed.
        case PM_EVT_SERVICE_CHANGED_IND_SENT:
        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
        default:
            break;
    }
}
/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_evt       Nordic UART Service event.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_evt_t * p_evt)
{
    //static SemaphoreHandle_t    reli_sema = NULL;
    //static SemaphoreHandle_t    reli_sema_stop = NULL;
    uint32_t ret_len = 0;
    uint8_t *p_data ;
    uint8_t ble_len = 0;
    switch(p_evt->type)
    {
        case BLE_NUS_EVT_RX_DATA:
        {
            //uint32_t err_code;
            //DBG_LOG("Received data from BLE NUS. Writing data on UART.");
            NRF_LOG_HEXDUMP_DEBUG(p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);
            p_data = (uint8_t *)p_evt->params.rx_data.p_data;
            ble_len = p_evt->params.rx_data.length;
            DBG_LOG("act slave received data len = %d",ble_len);
            ret_len = ble_combine_sub_frame((uint8_t *)slv_rx_buf,p_data,ble_len);
            if((ret_len > 0) && (queue_slv_rx != NULL))
            {                                
                xQueueSendToBackFromISR(queue_slv_rx, &ret_len, NULL);
                xQueueSendToBackFromISR(queue_slv_rx, (uint32_t *)&slv_rx_buf, NULL); 
                DBG_LOG("received total data len = %d",ret_len);
                DBG_LOG("data address = 0x%x",slv_rx_buf);
            }    
        }
        case BLE_NUS_EVT_COMM_STARTED:
            DBG_LOG("BLE_NUS_EVT_COMM_STARTED");
            //creat slave rx queue    
            //test_set_work_mode(START);
             
            break;
        case BLE_NUS_EVT_COMM_STOPPED:
            DBG_LOG("BLE_NUS_EVT_COMM_STOPPED");
            
            /*if(reli_sema)
            {
                xSemaphoreGive(reli_sema);
            }    */
            //test_stop();
            //test_set_work_mode(STOP);
            ble_disconnect_slave(0);
            //(void) sd_ble_gap_scan_stop();
            break;
        default : 
            break;

    }

}
/**@brief Callback handling NUS Client events.
 *
 * @details This function is called to notify the application of NUS client events.
 *
 * @param[in]   p_ble_nus_c   NUS Client Handle. This identifies the NUS client
 * @param[in]   p_ble_nus_evt Pointer to the NUS Client event.
 */

/**@snippet [Handling events from the ble_nus_c module] */
static void ble_nus_c_evt_handler(ble_nus_c_t * p_ble_nus_c, ble_nus_c_evt_t const * p_ble_nus_evt)
{
    static ble_pkg_t *c_rx_buf = NULL;
    uint32_t ret_len = 0;
    uint8_t *p_data ;
    uint8_t ble_len = 0;
    static QueueHandle_t c_rx_queue = NULL;  
    //portBASE_TYPE TaskWoken = pdFALSE;
    //BaseType_t xYieldRequired; 
    ret_code_t err_code;

    switch (p_ble_nus_evt->evt_type)
    {
        case BLE_NUS_C_EVT_DISCOVERY_COMPLETE:
            DBG_LOG("Discovery complete.");
            m_conn_handle_c = p_ble_nus_evt->conn_handle;
            err_code = ble_nus_c_handles_assign(p_ble_nus_c, p_ble_nus_evt->conn_handle, &p_ble_nus_evt->handles);
            APP_ERROR_CHECK(err_code);

            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
            APP_ERROR_CHECK(err_code);
            DBG_LOG("Connected to slave device");

            if(c_rx_queue == NULL)
            {
                c_rx_queue = ble_c_create_queue();
            }
            if(sema_c_s_connect != NULL)
            {
                //connect to slave
                xSemaphoreGive(sema_c_s_connect);
            }
            vPortFree(c_rx_buf);
            c_rx_buf = pvPortMalloc(BLE_RX_BUF_SIZE);
            break;           
        case BLE_NUS_C_EVT_NUS_TX_EVT:
                       
            p_data = p_ble_nus_evt->p_data;
            ble_len = p_ble_nus_evt->data_len;
            DBG_LOG("central received sub pkg data len = %d",ble_len);
        #if 0
            char str[256];
            memset(str,0,sizeof(str));
            HexToStr(str,p_data,ble_len);
            DBG_LOG("%s",str);
        #endif
            ret_len = ble_combine_sub_frame((uint8_t *)c_rx_buf,p_data,ble_len);
            if((ret_len > 0) && (c_rx_queue != NULL))
            {                                
                xQueueSendToBackFromISR(c_rx_queue, &ret_len, NULL);
                xQueueSendToBackFromISR(c_rx_queue, (uint32_t *)&c_rx_buf, NULL); 
                DBG_LOG("received total data len = %d",ret_len);
                DBG_LOG("data address = 0x%x",c_rx_buf);
            }    

            break;

        case BLE_NUS_C_EVT_DISCONNECTED:
            DBG_LOG("Central Disconnected slave.");
            vPortFree(c_rx_buf);
            c_rx_buf = NULL;
            m_conn_handle_c = BLE_CONN_HANDLE_INVALID;
            if(sema_c_s_connect != NULL)
            {
                xSemaphoreGive(sema_c_s_connect);
            }
            break;
    }
}
static void print_mac_addr(ble_gap_addr_t peer_addr)
{
    DBG_LOG("%02x:%02x:%02x:%02x:%02x:%02x\r\n",
                             peer_addr.addr[0],
                             peer_addr.addr[1],
                             peer_addr.addr[2],
                             peer_addr.addr[3],
                             peer_addr.addr[4],
                             peer_addr.addr[5]
                             );
}
static bool get_scan_advdata_name(uint8_t * p_encoded_data,
                           uint16_t        data_len,
                           char    const * p_target_name,
                           ble_gap_addr_t peer_addr)
{
    uint16_t   parsed_name_len;
    uint8_t  * p_parsed_name;
    uint16_t   data_offset          = 0;

    if (p_target_name == NULL)
    {
        return false;
    }


    parsed_name_len = ble_advdata_search(p_encoded_data,
                                         data_len,
                                         &data_offset,
                                         BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME);

    p_parsed_name = &p_encoded_data[data_offset];

    if ((data_offset != 0)&& (parsed_name_len != 0))
    {
       *(p_parsed_name+parsed_name_len)= 0;
        //return true;
        char *p_target = m_target_periph_name;
        char *p_scan = (char *)p_parsed_name;
        while((*p_target != '\0') && (*p_scan != '\0'))
        {
            if(*p_target != *p_scan)
            {
                return false;
            }
            p_target++;
            p_scan++;
        }
        if((p_target-m_target_periph_name) < strlen(m_target_periph_name))
        {
            return false;
        }
        DBG_LOG(" %s\r\n",p_parsed_name);
        print_mac_addr(peer_addr);
        return true;
        /*if(memcmp(p_parsed_name,m_target_periph_name,strlen(m_target_periph_name)) == 0)
        {
            return true;
        }*/
    }
    return false;
}
/*
static bool ble_find_target_mac(ble_gap_addr_t peer_addr)
{
    //slave_mac
    
    bool ret = false;
    if((peer_addr.addr[0] == slave_mac[0]) && \
       (peer_addr.addr[1] == slave_mac[1]) && \
       (peer_addr.addr[2] == slave_mac[2]) && \
       (peer_addr.addr[3] == slave_mac[3]) && \
       (peer_addr.addr[4] == slave_mac[4]) && \
       (peer_addr.addr[5] == slave_mac[5]))
       {
           ret = true;
       }
    return ret;
}*/
/**@brief Function for handling the advertising report BLE event.
 *
 * @param[in] p_adv_report  Advertising report from the SoftDevice.
 */
static void on_adv_report(ble_gap_evt_adv_report_t const * p_adv_report)
{
    //ret_code_t err_code;
    /*DBG_LOG("%02x:%02x:%02x:%02x:%02x:%02x\r\n",
                             p_adv_report->peer_addr.addr[0],
                             p_adv_report->peer_addr.addr[1],
                             p_adv_report->peer_addr.addr[2],
                             p_adv_report->peer_addr.addr[3],
                             p_adv_report->peer_addr.addr[4],
                             p_adv_report->peer_addr.addr[5]
                             );*/
    bool ret = get_scan_advdata_name(p_adv_report->data.p_data, \
                                  p_adv_report->data.len, \
                                  m_target_periph_name, \
                                  p_adv_report->peer_addr);
    if(ret)
    {
        mac_addr_save((ble_gap_addr_t *)&p_adv_report->peer_addr);
        /*if(sema_scan != NULL)
        {
            xSemaphoreGive(sema_scan);
        }*/
        
    }
    //if (ble_advdata_name_find(p_adv_report->data.p_data,
    //                              p_adv_report->data.len,
    //                              m_target_periph_name))
    /*
    if(ble_find_target_mac(p_adv_report->peer_addr))
    {
            // Initiate connection.
            err_code = sd_ble_gap_connect(&p_adv_report->peer_addr,
                                          &m_scan_params,
                                          &m_connection_param,
                                          APP_BLE_CONN_CFG_TAG);
            
            if (err_code != NRF_SUCCESS)
            {
                DBG_LOG("Connection Request Failed, reason %d", err_code);
            }
    }
    else
    {
       scan_start();
    }
    */
   scan_start();
}


/**@brief Function for assigning new connection handle to available instance of QWR module.
 *
 * @param[in] conn_handle New connection handle.
 */

static void multi_qwr_conn_handle_assign(uint16_t conn_handle)
{
    for (uint32_t i = 0; i < NRF_SDH_BLE_TOTAL_LINK_COUNT; i++)
    {
        if (m_qwr[i].conn_handle == BLE_CONN_HANDLE_INVALID)
        {
            ret_code_t err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr[i], conn_handle);
            APP_ERROR_CHECK(err_code);
            break;
        }
    }
    //nrf_ble_qwr_conn_handle_assign(m_qwr, conn_handle);
}


/**@brief   Function for handling BLE events from central applications.
 *
 * @details This function parses scanning reports and initiates a connection to peripherals when a
 *          target UUID is found. It updates the status of LEDs used to report central applications
 *          activity.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_central_evt(ble_evt_t const * p_ble_evt)
{
    static SemaphoreHandle_t   sema_c_tx_complete = NULL;
    ret_code_t            err_code;
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        // Upon connection, check which peripheral has connected (HR or RSC), initiate DB
        // discovery, update LEDs status and resume scanning if necessary.
        case BLE_GAP_EVT_CONNECTED:
        {
            DBG_LOG("+++++++++++++++++++ Central connected ++++++++++++++++++++");
            err_code = ble_nus_c_handles_assign(&m_nus_c, p_ble_evt->evt.gap_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);
            // start discovery of services. The NUS Client waits for a discovery result
            err_code = ble_db_discovery_start(&m_db_discovery, p_ble_evt->evt.gap_evt.conn_handle);
            APP_ERROR_CHECK(err_code);
            // Assing connection handle to the QWR module.
            multi_qwr_conn_handle_assign(p_gap_evt->conn_handle);

            if(sema_c_tx_complete == NULL)
            {
                sema_c_tx_complete = ble_c_create_sema();
                xSemaphoreTake( sema_c_tx_complete, 0 );
            }
            
        } break; // BLE_GAP_EVT_CONNECTED

        // Upon disconnection, reset the connection handle of the peer which disconnected,
        // update the LEDs status and start scanning again.
        case BLE_GAP_EVT_DISCONNECTED:
        {
            DBG_LOG("The central disconnected (reason: %d)",
                             p_gap_evt->params.disconnected.reason);
                             
            m_conn_handle_c = BLE_CONN_HANDLE_INVALID;
            vSemaphoreDelete( sema_c_tx_complete );
            sema_c_tx_complete = NULL;
            if(sema_c_s_connect != NULL)
            {
                //will not be perform after connected slave
                xSemaphoreGive(sema_c_s_connect);
            }
        } break; // BLE_GAP_EVT_DISCONNECTED

        case BLE_GAP_EVT_ADV_REPORT:
        {
            on_adv_report(&p_gap_evt->params.adv_report);
        } break; // BLE_GAP_ADV_REPORT

        case BLE_GAP_EVT_TIMEOUT:
        {
            // We have not specified a timeout for scanning, so only connection attemps can timeout.
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                DBG_LOG("Connection Request timed out.");
            }
        } break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        {
            // Accept parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                        &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);

        } break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            DBG_LOG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            DBG_LOG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            DBG_LOG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_GATTC_EVT_WRITE_CMD_TX_COMPLETE:
            DBG_LOG("BLE_GATTC_EVT_WRITE_CMD_TX_COMPLETE");
            xSemaphoreGive(sema_c_tx_complete);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief   Function for handling BLE events from peripheral applications.
 * @details Updates the status LEDs used to report the activity of the peripheral applications.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_peripheral_evt(ble_evt_t const * p_ble_evt)
{
    ret_code_t err_code;
    static SemaphoreHandle_t   sema_s_tx_complete = NULL;
    //static TaskHandle_t         task_handle = NULL;
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            DBG_LOG("Peripheral connected");
            
            //bsp_board_led_off(PERIPHERAL_ADVERTISING_LED);
            //bsp_board_led_on(PERIPHERAL_CONNECTED_LED);
            m_conn_handle_s = p_ble_evt->evt.gap_evt.conn_handle;
            // Assing connection handle to the QWR module.
            multi_qwr_conn_handle_assign(p_ble_evt->evt.gap_evt.conn_handle);
            if(sema_s_tx_complete == NULL)
            {
                sema_s_tx_complete = ble_s_create_sema();
            }
            if(queue_slv_rx == NULL)
            {
                queue_slv_rx = ble_s_create_queue();
                xQueueReset(queue_slv_rx);
            }
            //rx memeory
            vPortFree(slv_rx_buf);
            slv_rx_buf = pvPortMalloc(BLE_RX_BUF_SIZE);
            
            //nrf_ble_qwr_conn_handle_assign(&m_qwr, p_ble_evt->evt.gap_evt.conn_handle);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            DBG_LOG("Peripheral disconnected");
            ble_disconnect_slave(0);
            //bsp_board_led_off(PERIPHERAL_CONNECTED_LED);
            m_conn_handle_s = BLE_CONN_HANDLE_INVALID;
            vSemaphoreDelete(sema_s_tx_complete);
            sema_s_tx_complete = NULL;
            vPortFree(slv_rx_buf);
            vQueueDelete(queue_slv_rx);
            scan_stop();
            slv_rx_buf = NULL;
            queue_slv_rx = NULL;
            if(sema_c_s_connect != NULL)
            {
                xSemaphoreGive(sema_c_s_connect);
            }
            test_set_work_mode(STOP);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            DBG_LOG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            DBG_LOG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            DBG_LOG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
            DBG_LOG("BLE_GATTS_EVT_HVN_TX_COMPLETE");
            if(sema_s_tx_complete != NULL)
            {
                xSemaphoreGive(sema_s_tx_complete);
            }
            break;
        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for handling advertising events.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
        {
            DBG_LOG("Fast advertising.");
            //bsp_board_led_on(PERIPHERAL_ADVERTISING_LED);
        } break;

        case BLE_ADV_EVT_IDLE:
        {
            ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
            APP_ERROR_CHECK(err_code);
        } break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for checking if a bluetooth stack event is an advertising timeout.
 *
 * @param[in] p_ble_evt Bluetooth stack event.
 */
static bool ble_evt_is_advertising_timeout(ble_evt_t const * p_ble_evt)
{
    return (p_ble_evt->header.evt_id == BLE_GAP_EVT_ADV_SET_TERMINATED);
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint16_t conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    uint16_t role        = ble_conn_state_role(conn_handle);

    // Based on the role this device plays in the connection, dispatch to the right handler.
    if (role == BLE_GAP_ROLE_PERIPH || ble_evt_is_advertising_timeout(p_ble_evt))
    {
        //ble_nus_on_ble_evt(p_ble_evt, &m_nus);
        on_ble_peripheral_evt(p_ble_evt);
    }
    else if ((role == BLE_GAP_ROLE_CENTRAL) || (p_ble_evt->header.evt_id == BLE_GAP_EVT_ADV_REPORT))
    {
        ble_nus_c_on_ble_evt(p_ble_evt, &m_nus_c);
        on_ble_central_evt(p_ble_evt);
    }
}

/**@brief Function for initializing the NUS Client. */
static void nus_c_init(void)
{
    ret_code_t       err_code;
    ble_nus_c_init_t init;

    init.evt_handler = ble_nus_c_evt_handler;

    err_code = ble_nus_c_init(&m_nus_c, &init);
    APP_ERROR_CHECK(err_code);
}
/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupts.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for the Peer Manager initialization.
 */
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Clear bond information from persistent storage.
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    DBG_LOG("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to
 *                            wake the application up.
 */
/*
static void buttons_leds_init(bool * p_erase_bonds)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, NULL);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}
*/

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONNECTION_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONNECTION_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = SUPERVISION_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle_s == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        ble_s_max_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        DBG_LOG("\r\nData len is set to 0x%X(%d)\r\n", ble_s_max_len, ble_s_max_len);
    }
}
/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_CONN_HANDLE_INVALID; // Start upon connection.
    cp_init.disconnect_on_fail             = true;
    cp_init.evt_handler                    = NULL;  // Ignore events.
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling database discovery events.
 *
 * @details This function is callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function should forward the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    //ble_hrs_on_db_disc_evt(&m_hrs_c, p_evt);
    //ble_rscs_on_db_disc_evt(&m_rscs_c, p_evt);
    ble_nus_c_on_db_disc_evt(&m_nus_c, p_evt);
}


/**
 * @brief Database discovery initialization.
 */
static void db_discovery_init(void)
{
    ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing services that will be used by the application.
 *
 * @details Initialize the Heart Rate, Battery and Device Information services.
 */
static void services_init(void)
{
    uint32_t           err_code;
    ble_nus_init_t     nus_init;
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    for (uint32_t i = 0; i < NRF_SDH_BLE_TOTAL_LINK_COUNT; i++)
    {
        err_code = nrf_ble_qwr_init(&m_qwr[i], &qwr_init);
        APP_ERROR_CHECK(err_code);
    }

    // Initialize NUS.
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;

    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
#if USER_DUF_ENABLE
    ble_dfu_buttonless_init_t dfus_init = {0};
    // Initialize the async SVCI interface to bootloader.
    err_code = ble_dfu_buttonless_async_svci_init();
    APP_ERROR_CHECK(err_code);

    dfus_init.evt_handler = ble_dfu_evt_handler;

    err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);
  
#endif
}


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    ret_code_t             err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}


/**@brief Function for starting advertising. */
static void advertising_start(void * p_erase_bonds)
{
    bool erase_bonds = *(bool*)p_erase_bonds;

    if (erase_bonds)
    {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETE_SUCCEEDED event.
    }
    else
    {
        ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
        APP_ERROR_CHECK(err_code);
    }
}
uint32_t ble_act_central_send(void * const p_data, uint16_t length)
{    
    if(m_conn_handle_c == BLE_CONN_HANDLE_INVALID)
    {
        return 0;
    }
    return ble_nus_c_string_send(&m_nus_c,p_data,length);    
}
uint32_t ble_act_slave_send(void * const p_data, uint16_t length)
{
    if(m_conn_handle_s == BLE_CONN_HANDLE_INVALID)
    {
        return 0;
    }
    return ble_nus_data_send(&m_nus,p_data,&length,m_conn_handle_s);
}

void ble_init(bool *erase_bonds)
{
    ble_stack_init();
    gap_params_init();
    gatt_init();
    conn_params_init();
    db_discovery_init();
    peer_manager_init();
    nus_c_init();
    services_init();
    advertising_init();

    nrf_sdh_freertos_init(advertising_start, erase_bonds);
}

