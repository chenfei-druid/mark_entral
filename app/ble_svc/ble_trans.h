#ifndef BLE_TRANS_H
#define BLE_TRANS_H

#include "FreeRTOS.h"
#include "semphr.h"
#include "queue.h"

#define     BLE_TX_COMPLETE_WAIT    4000     //MS
#define     BLE_S_MAX_LEN           20

QueueHandle_t ble_c_create_queue(void);
QueueHandle_t ble_s_create_queue(void);
SemaphoreHandle_t ble_c_create_sema(void);
SemaphoreHandle_t ble_s_create_sema(void);
uint32_t ble_c_receive(void * const buffer, uint16_t max_size, uint32_t timeout);
uint32_t ble_c_send(void * const p_data, uint16_t length);
uint32_t ble_s_send(void * const p_buf, uint16_t const length);
uint32_t ble_s_pkg_send(void * const buffer, uint16_t length);
uint32_t ble_slave_receive(void * const buffer, uint16_t max_size, uint32_t timeout);
#endif //BLE_TRANS_H
