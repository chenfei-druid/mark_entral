/**
 * @brief 
 * 
 * @file ble_pkg.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-12
 */
#include "stdlib.h"
#include "ble_pkg.h"
#include "user_app_log.h"
#include "crc16.h"

static uint32_t ble_get_new_sub_frame(uint8_t **p_dst_final, 
                                           uint8_t const *p_src_new_frame, 
                                           uint8_t frame_len)
{
    if(!*p_dst_final || !p_src_new_frame)
    {
        DBG_LOG("NULL Pointer\r\n");
        return 0;
    }
    memcpy(*p_dst_final,p_src_new_frame,frame_len);
    *p_dst_final += frame_len;
    return  frame_len;
}
uint32_t ble_get_valid_field(uint8_t *p_dst, uint8_t const *p_src, uint32_t length)
{
    if(!p_dst|| !p_src)
    {
        DBG_LOG("\r\np_dest or p_data is NULL\r\n");
        return 0;
    }
    
    ble_pkg_head_t *ble_pkg_head = (ble_pkg_head_t *) p_src;
    if(length < (sizeof(ble_pkg_head_t)+ble_pkg_head->data_len))
    {
        DBG_LOG("\r\ncommand length is not enough\r\n");
        return 0;
    }

    uint16_t *p_giv_crc = (uint16_t *)((void *)(p_src + sizeof(ble_pkg_head_t) + ble_pkg_head->data_len));
    uint16_t crc_cal = crc16_compute(p_src, sizeof(ble_pkg_head_t) + ble_pkg_head->data_len,NULL);
    if( crc_cal != *p_giv_crc )
    {
        DBG_LOG("\r\nCrc is wrong, given crc = 0x%x , calcrc = 0x%x \r\n",*p_giv_crc,crc_cal);
        DBG_LOG("\r\nCrc caculate len = %d \r\n",sizeof(ble_pkg_head_t) + ble_pkg_head->data_len);
    #if 0
        for(uint8_t i=0;i<sizeof(ble_pkg_head_t) + ble_pkg_head->data_len;i++)
        {
            DBG_D("%x ",p_src[i]);
        }
        DBG_D("\r\n")
    #endif
        return 0;
    }
    memcpy(p_dst, p_src + sizeof(ble_pkg_head_t),ble_pkg_head->data_len);
    return ble_pkg_head->data_len;
}
uint16_t ble_combine_sub_frame(uint8_t *p_dest_frame, const uint8_t *p_split_frame, int16_t split_len)
{
    if(split_len == 0)
    {
        DBG_LOG("ble length == 0\r\n");
        return 0;
    }
    if(!p_split_frame || !p_dest_frame)
    {
        DBG_LOG("ble_combine_sub_frame pointer is NULL\r\n");
        return 0;
    }
    int16_t frame_len = 0;
    split_head_t *p_split_head = (split_head_t *)(p_split_frame);
    static construct_mode_t current_construct_mode = CONSTRUCT_MODE_INVALID;
    static uint8_t  *p_current_pos = NULL;

    
    //DBG_LOG("p_split_head->type is: %d\r\n", p_split_head->type);

    switch(p_split_head->type)
    {
        case SPLIT_FRAME_TYPE_MIDDLE:
        {
            //DBG_LOG("this is middle frame type\r\n");

            if(current_construct_mode == CONSTRUCT_MODE_CONSTRUCTING)
            {
                // calucate current positon to story
                // copy data to dest
                ble_get_new_sub_frame(&p_current_pos,
                                                  p_split_frame + sizeof(split_head_t),
                                                  split_len - sizeof(split_head_t)
                                                 );
                //DBG_LOG("middle frame len is:%d\r\n",p_current_pos - p_dest_frame );
            }
        }
            break;
        case SPLIT_FRAME_TYPE_START:
        {
            //DBG_LOG("this is first frame type\r\n");
            current_construct_mode = CONSTRUCT_MODE_CONSTRUCTING;
            p_current_pos = p_dest_frame;
            ble_get_new_sub_frame(&p_current_pos,
                                                  p_split_frame + sizeof(split_head_t),
                                                  split_len - sizeof(split_head_t)
                                                 );            
            //DBG_LOG("first frame len is:%d\r\n", p_current_pos - p_dest_frame);
            // update destination
            break;
        }
        
        case SPLIT_FRAME_TYPE_LAST:
        {
            //DBG_LOG("this is last frame type\r\n");
            if(current_construct_mode == CONSTRUCT_MODE_CONSTRUCTING)
            {
                // calucate current positon to story
                // copy data to dest
                ble_get_new_sub_frame(&p_current_pos,
                                                    p_split_frame + sizeof(split_head_t),
                                                    split_len - sizeof(split_head_t)
                                                    );
                current_construct_mode = CONSTRUCT_MODE_CONSTRUCTED;                            
                frame_len = p_current_pos - p_dest_frame;
                //DBG_LOG("lase frame len is:%d\r\n", frame_len);
            }
        }
            break;
        case SPLIT_FRAME_TYPE_TOTAL:
        {
            DBG_LOG("this is total frame type\r\n");
            p_current_pos = p_dest_frame;
            memcpy(p_current_pos, p_split_frame + sizeof(split_head_t), split_len - sizeof(split_head_t));
            current_construct_mode = CONSTRUCT_MODE_CONSTRUCTED;
            frame_len = split_len - sizeof(split_head_t);           
            break;
        }
        default:
            break;
    }
    DBG_LOG("total frame len is:%d\r\n", frame_len);
    return frame_len;
}
uint32_t ble_package(uint8_t * const p_dst,
                    uint8_t const *p_src, 
                     uint32_t const dst_size,
                     uint32_t const src_len)
{
    if(p_dst == NULL)
    {
        DBG_LOG("ble package out pointer is NULL\r\n");
        return 0;
    }
    if((sizeof(ble_pkg_head_t)+src_len+2) > dst_size)
    {       
        DBG_LOG("ble package length is more than out size\r\n");
        return 0;
    }
    ble_pkg_t   ble_pkg;
    static uint8_t seq = 0;
    seq++;
    if(seq == 0)
    {
        seq++;
    }
    ble_pkg.head.sequence  = seq;
    ble_pkg.head.func_code = CMD;
    ble_pkg.head.data_len  = src_len;    
    ble_pkg.frame          = (uint8_t *)p_src;

    //copy all data into out buffer
    memcpy(p_dst,(uint8_t *)&ble_pkg.head,sizeof(ble_pkg.head));
    memcpy(p_dst+sizeof(ble_pkg.head),(uint8_t *)ble_pkg.frame,src_len);

    uint16_t crc16 = crc16_compute(p_dst,src_len + sizeof(ble_pkg.head),NULL);

    memcpy(p_dst+sizeof(ble_pkg.head)+src_len,(uint8_t *)&crc16,2);

    return (src_len + sizeof(ble_pkg.head) + 2);
}