/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.6-dev at Wed Aug 08 10:39:09 2018. */

#ifndef PB_BLE_CONN_PB_H_INCLUDED
#define PB_BLE_CONN_PB_H_INCLUDED
#include <pb.h>

#include "IdentityMsg.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Enum definitions */
typedef enum
{
    PROTOCOL_CMD_TYPE_SENDCMD = 0,
    PROTOCOL_CMD_TYPE_CONNECT = 1,
    PROTOCOL_CMD_TYPE_DISCONNECT = 2
} protocol_cmd_type_t;
#define PROTOCOL_CMD_TYPE_MIN PROTOCOL_CMD_TYPE_SENDCMD
#define PROTOCOL_CMD_TYPE_MAX PROTOCOL_CMD_TYPE_DISCONNECT
#define PROTOCOL_CMD_TYPE_ARRAYSIZE ((protocol_cmd_type_t)(PROTOCOL_CMD_TYPE_DISCONNECT+1))

/* Struct definitions */
typedef struct {
    protocol_cmd_type_t Type;
    bool has_Head;
    protocol_identity_msg_t Head;
    pb_callback_t MAC;
/* @@protoc_insertion_point(struct:protocol_ble_conn_req_t) */
} protocol_ble_conn_req_t;

/* Default values for struct fields */

/* Initializer values for message structs */
#define PROTOCOL_BLE_CONN_REQ_INIT_DEFAULT       {(protocol_cmd_type_t)0, false, PROTOCOL_IDENTITY_MSG_INIT_DEFAULT, {{NULL}, NULL}}
#define PROTOCOL_BLE_CONN_REQ_INIT_ZERO          {(protocol_cmd_type_t)0, false, PROTOCOL_IDENTITY_MSG_INIT_ZERO, {{NULL}, NULL}}

/* Field tags (for use in manual encoding/decoding) */
#define PROTOCOL_BLE_CONN_REQ_TYPE_TAG           1
#define PROTOCOL_BLE_CONN_REQ_HEAD_TAG           2
#define PROTOCOL_BLE_CONN_REQ_MAC_TAG            3

/* Struct field encoding specification for nanopb */
extern const pb_field_t protocol_ble_conn_req_fields[4];

/* Maximum encoded size of messages (where known) */
/* PROTOCOL_BLE_CONN_REQ_SIZE depends on runtime parameters */

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define BLE_CONN_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
