
/** 
 * user_data_pkg.c
 *
 * @group:       nack_trap project
 * @author:      Chenfei
 * @create time: 2018/4/2
 * @version:     V0.0
 *
 */
#include <stdint.h>

#include <string.h>
#include "user_data_pkg.h"
//#include "ble_application.h"
#include "crc16.h"
#include "user_app_log.h"

/*
static uint16_t user_crc16_compute(uint8_t const * p_data, uint32_t size, uint16_t const * p_crc)
{
    uint16_t crc = (p_crc == NULL) ? 0xFFFF : *p_crc;

    for (uint32_t i = 0; i < size; i++)
    {
        crc  = (uint8_t)(crc >> 8) | (crc << 8);
        crc ^= p_data[i];
        crc ^= (uint8_t)(crc & 0xFF) >> 4;
        crc ^= (crc << 8) << 4;
        crc ^= ((crc & 0xFF) << 4) << 1;
    }
    return crc;
}
*/
/**
* @functionname:    user_add_head_to_protbuf
* @description  :   function for adding the head data into protobuf frame, @ref pkg_prot_frame_t
* _____________________________________________________________________________________________________________|___________________
*----------------------------------------head------------------------------------------------------------------|
* | protocal-version | vendor-id | command-type |   data-len   | encryption-select | reserve |   crc16         | protobuf-data
* |  accumulation    |   start   |              |  protobuf    |                   |         |caculate all     |
* |    from 1        |  from 1   |              | data length  |                   |         |data except self |
* ________________________________________________________________________________________________________________________________
*
* @input        :    protbuf_encode_data, pointor to data buffer, protbuf_length: the length of protobuf data after encode 
* @output       :    p_prot_frame_t: the struct include all head and protocal data
* @return       :    true, successful, false, failed
*/
uint16_t user_add_head_to_protbuf(pkg_prot_frame_t * p_prot_frame_t, 
                                  uint8_t * const protbuf_encode_data, 
                                  int16_t const protbuf_length,
                                  uint16_t cmd_type
                                  )
{
    if(p_prot_frame_t == NULL || protbuf_encode_data == NULL)
    {        
        DBG_LOG("\r\nadd package head of pointer is null !\r\n");   
        return 0;
    }
    if(protbuf_length <= 0)
    {
        DBG_LOG("\r\ninvalid proto buf length !\r\n");
        return 0;
    }
    p_prot_frame_t->prot_head.prot_version      = PROT_VER;
    p_prot_frame_t->prot_head.vendor_id         = VENDOR_ID;
    p_prot_frame_t->prot_head.cmd_type          = cmd_type;
    p_prot_frame_t->prot_head.frame_len         = protbuf_length;
    p_prot_frame_t->prot_head.option_encryption = OPTION_ENCRYPTION;
    p_prot_frame_t->prot_head.reserve           = 0;
    //put data into buffer
    p_prot_frame_t->p_prot_data                 = protbuf_encode_data;
    uint16_t crc16 = 0;   
    
    int16_t head_len = sizeof(pkg_prot_head_t)-sizeof(p_prot_frame_t->prot_head.crc16);
    
    crc16 = crc16_compute((uint8_t *)&p_prot_frame_t->prot_head, head_len,NULL);   
    p_prot_frame_t->prot_head.crc16             = crc16_compute(p_prot_frame_t->p_prot_data, p_prot_frame_t->prot_head.frame_len, &crc16);    
       
    //p_prot_frame_t->prot_head.crc16               = user_crc16_compute(protbuf_encode_data, protbuf_length, NULL);
    //DBG_LOG("\r\n$$$$$$$$packege protobuf crc16 = 0x%x $$$$$$$$$$$$$$\r\n",p_prot_frame_t->prot_head.crc16);   
    //memcpy(p_prot_frame_t->p_prot_data , protbuf_encode_data, protbuf_length);
    uint16_t len =  sizeof(pkg_prot_head_t) + protbuf_length;
   /* uint8_t *p = (uint8_t *)&p_prot_frame_t->prot_head;
    
    DBG_LOG("\r\n protocal frame:");
    for(uint16_t i=0;i<head_len;i++)
    {
        DBG_LOG("%02x ",p[i]);
    }
    p = p_prot_frame_t->p_prot_data;
    for(uint16_t i=0;i<p_prot_frame_t->prot_head.frame_len;i++)
    {
        DBG_LOG("%02x ",p[i]);
    }
    */
    return len;
}
/**
* @functionname:    user_package_final_frame
* @description  :   function for adding the head data into final upload frame, @ref pkg_final_frame_trans_t, @ref pkg_final_frame_head_t
* _____________________________________________________________________________________________________________|___________________
*----------------------------------------head------------------------------------------------------------------|
* | frame number | func code cmd |   data-len              |    data       |        crc16         | 
* |    from 1    |               | only data filed length  |    ...        | all data except self                | 
* ________________________________________________________________________________________________________________________________
*
* @input        :    p_prot_frame_t, the data pointer will be packaged , frame_len: packaged data length 
* @output       :    p_final_frame_t: the pointor to final send data frame, 
* @return       :    true, successful, false, failed
*/
uint16_t user_package_final_frame(pkg_final_frame_trans_t * p_final_frame_t, 
                                  pkg_prot_frame_t * p_prot_frame_t, 
                                  const int16_t frame_len,
                                  uint8_t sequence,
                                  uint8_t func_code
                                  )
{
    if(frame_len <= 0)
    {
        return 0;
    }
//    uint8_t *p_frame = (uint8_t *)p_prot_frame_t;

    p_final_frame_t->head.sequence  = sequence;
    p_final_frame_t->head.func_code = func_code;
    p_final_frame_t->head.data_len  = frame_len;
    
    p_final_frame_t->frame          = p_prot_frame_t;  
    
    return (frame_len + sizeof(p_final_frame_t->head));
}
/**
* @functionname:    user_put_final_frame_into_buf
* @description  :   function for copy final frame to buffer

* @input        :    p_final_frame_t: source whole package data will be sent 
* @output       :    p_buf: the pointor to send data buffer, 
* @return       :    data length to be sent
*/
uint32_t user_put_final_frame_into_buf(uint8_t *p_buf, pkg_final_frame_trans_t * p_final_frame_t)
{
    uint32_t cnt =0;
    pkg_prot_frame_t * p_prot_frame = p_final_frame_t->frame;
    //put final package head into buffer
    memcpy(p_buf, &p_final_frame_t->head,sizeof(p_final_frame_t->head));
    cnt += sizeof(p_final_frame_t->head);
    //DBG_LOG("\r\ncnt + p_final_frame_t->head len = %d\r\n",cnt);
    //put final package frame field into buffer, the frame has two filed, protocol head and protocol data field
    //this step is copy protocol head
    memcpy(&p_buf[cnt], &p_final_frame_t->frame->prot_head,sizeof(p_prot_frame->prot_head));
    cnt += sizeof(p_prot_frame->prot_head);
    //DBG_LOG("\r\ncnt + p_prot_frame->prot_head = %d\r\n",cnt);
    //put protocol data field
    memcpy(&p_buf[cnt], p_final_frame_t->frame->p_prot_data,p_prot_frame->prot_head.frame_len);
    cnt += p_prot_frame->prot_head.frame_len;
    //DBG_LOG("\r\ncnt + p_prot_frame->prot_head.frame_len = %d\r\n",cnt);
    //put final package crc
    //memcpy(&p_buf[cnt], &p_final_frame_t->crc16,sizeof(p_final_frame_t->crc16));
    //cnt += sizeof(p_final_frame_t->crc16);
    return cnt;
}
/**
* @functionname:    user_put_protocal_frame_into_buf
* @description  :   function for copy final frame to buffer

* @input        :    p_final_frame_t: source whole package data will be sent 
* @output       :    p_buf: the pointor to send data buffer, 
* @return       :    data length to be sent
*/
static uint32_t user_put_protocal_frame_into_buf(uint8_t *p_buf, uint32_t out_buf_size, pkg_prot_frame_t * p_prot_frame_t)
{
    uint32_t cnt =0;
    if(p_buf == NULL || p_prot_frame_t == NULL)
    {
        DBG_LOG("\r\npackage pointer is Null!\r\n");
        return 0;
    }
    if(p_prot_frame_t->prot_head.frame_len > (out_buf_size - sizeof(pkg_prot_head_t)))
    {
        p_prot_frame_t->prot_head.frame_len = out_buf_size - sizeof(pkg_prot_head_t);
        DBG_LOG("\r\nframe data length exceed max size, force = max size: %d\r\n",out_buf_size);
    }
    //put protocal package head into buffer
    memcpy(p_buf, &p_prot_frame_t->prot_head,sizeof(pkg_prot_head_t));
    cnt += sizeof(pkg_prot_head_t);
    //DBG_LOG("\r\ncnt + p_final_frame_t->head len = %d\r\n",cnt);
    //put final package frame field into buffer, the frame has two filed, protocol head and protocol data field
    //this step is copy protocol head
    memcpy(&p_buf[cnt], p_prot_frame_t->p_prot_data,p_prot_frame_t->prot_head.frame_len);
    cnt += p_prot_frame_t->prot_head.frame_len;
    //DBG_LOG("\r\ncnt + p_prot_frame->prot_head = %d\r\n",cnt);

    return cnt;
}
/**
* @functionname : user_get_sub_pkg_from_cache 
* @description  : function for splitting the datas to sub package  to send
* @input        : buffer: pointor to data buf, length: data length will be sent
* @output       : none 
* @return       : true if successed
*/
uint16_t user_get_sub_pkg_from_cache(uint8_t const * const src_buf, 
                                     uint8_t *dst_buf, 
                                     uint16_t const curren_len,
                                     uint16_t const total_len,
                                     uint16_t const split_pkg_len)
{
    uint16_t valid_len;
    
    if(curren_len > total_len || \
       curren_len == 0 || \
       total_len  == 0 || \
       split_pkg_len == 0)
    {
        return 0;
    }
    if(src_buf == NULL || dst_buf == NULL)
    {
        return 0;
    }
    //add header and copy data
    if(curren_len >= split_pkg_len)
    {
        dst_buf[0] = (curren_len == total_len)? HEAD_CMD_FIRST_PACKET : HEAD_CMD_MIDDLE_PACKET;
        valid_len = split_pkg_len ;
    }
    else
    {        
        //only (or remain) signle package
        dst_buf[0] = (curren_len == total_len)? HEAD_CMD_SINGLE_PACKET : HEAD_CMD_LAST_PACKET;
        valid_len = curren_len;        
    }
    memcpy(&dst_buf[1], src_buf, valid_len); 
    //DBG_LOG("\r\n current split package, len = %d, header= 0x%2x\r\n", valid_len,dst_buf[0]);
    /*
    DBG_LOG("\r\ndata before copy:\r\n");
    for(uint8_t i=0;i<curren_len;i++)
    {
        DBG_LOG("%x ",src_buf[i]);
    }
    DBG_LOG("\r\ndata after copy:\r\n");
    for(uint8_t i=0;i<curren_len+1;i++)
    {
        DBG_LOG("%x ",dst_buf[i]);
    }
    */
    return valid_len;
    
}
/**
* @functionname : user_ble_data_packge 
* @description  : packge the all data
* @input        : 
*               : p_src: pointor to source data, 
*               : byte_len: length for package
*               : cmd_type: protocol data type @ref protocol_header_type_t
* @output       : p_dst: pointor to dest buffer
* @return       : return package length
*/
uint32_t user_ble_data_packge(  uint8_t *const p_dst, int32_t out_buf_size, uint8_t *p_src, int32_t byte_len, 
                            uint16_t cmd_type, uint8_t sequence, uint8_t func_code
                         )
{
    if(p_dst == NULL || p_src == NULL)
    {
        DBG_LOG("\r\nble package pointer is null !\r\n");
        return 0;
    }
    if(byte_len <=0)
    {
        DBG_LOG("\r\nble package byte length is null !\r\n");
        return 0;
    }
    pkg_prot_frame_t  p_prot_frame_t;
    pkg_final_frame_trans_t  p_final_frame_t;
    
    uint32_t len_ret;
    len_ret = user_add_head_to_protbuf(&p_prot_frame_t, p_src, byte_len, cmd_type);
    if( len_ret == 0 )
    {
        DBG_LOG("\r\nble package add head return invalide length !\r\n");
        return 0;
    }
    len_ret = user_package_final_frame(&p_final_frame_t, &p_prot_frame_t, len_ret, sequence, func_code);
    if( len_ret == 0 )
    {
        DBG_LOG("\r\nble package final frame return invalid length !\r\n");
        return 0;
    }
        
    len_ret = user_put_final_frame_into_buf(p_dst,&p_final_frame_t);
    p_final_frame_t.crc16 = crc16_compute(p_dst, len_ret, NULL);
    memcpy(&p_dst[len_ret],(uint8_t *)&p_final_frame_t.crc16,sizeof(p_final_frame_t.crc16));
    //DBG_LOG("\r\n##############crc16 = 0x%x #######crc length = %d\r\ncrc content:\r\n",p_final_frame_t.crc16, len_ret );
    /*
    for(uint8_t i=0;i<len_ret;i++)
    {
        DBG_LOG("%x ",p_dst[i]);
    }
    */
    len_ret += sizeof(p_final_frame_t.crc16);
    //DBG_LOG("\r\nble package len_ret = %d\r\n",len_ret);
    return len_ret;
}
/**
* @functionname : user_nb_data_packge 
* @description  : packge the all data for nb communication channel
* @input        : 
*               : p_src: pointor to source data, 
*               : byte_len: length for package
*               : cmd_type: protocol data type @ref protocol_header_type_t
* @output       : p_dst: pointor to dest buffer
* @return       : return package length
*/
uint32_t user_nb_data_packge(  uint8_t * const p_dst, int32_t out_buf_size, uint8_t *p_src, int32_t byte_len, uint16_t cmd_type)
{
    if(byte_len <=0)
    {
        return false;
    }
    pkg_prot_frame_t  p_prot_frame_t;

    uint32_t len_ret;
    len_ret = user_add_head_to_protbuf(&p_prot_frame_t, p_src, byte_len, cmd_type); 
    
    len_ret = user_put_protocal_frame_into_buf(p_dst,out_buf_size,&p_prot_frame_t);
    if(len_ret > byte_len + sizeof(pkg_prot_head_t))
    {
        DBG_LOG("\r\nnb package return invalid length %d > %d\r\n",len_ret,byte_len + sizeof(pkg_prot_head_t));
        return 0;
    }
    /*
    DBG_LOG("\r\nuser_nb_data_packge content:\r\n");
    for(uint8_t i=0;i<len_ret;i++)
    {
        DBG_LOG("%02x ",p_dst[i]);
    }
    */
    DBG_LOG("\r\nnb-iot package len_ret = %d\r\n",len_ret);
    return len_ret;
}

/**
* @functionname : user_data_packge 
* @description  : packge the all data according to interface channel
* @input        : 
*               : p_src: pointor to source data, 
*               : byte_len: length for package
*               : cmd_type: protocol data type @ref protocol_header_type_t
* @output       : p_dst: pointor to dest buffer
* @return       : return package length
*/
uint32_t user_data_packge(  uint8_t *const p_dst, int32_t out_buf_size, uint8_t *p_src, int32_t byte_len, 
                            uint16_t cmd_type, uint8_t sequence, uint8_t func_code,
                            trans_interface_type chn_type
                         )
{
    uint32_t ret;
    switch(chn_type)
    {
        case COMMUNICATION_INTERFACE_TYPE_NB:
            ret = user_nb_data_packge(p_dst, out_buf_size, p_src, byte_len, cmd_type);      
            break;
        
        case COMMUNICATION_INTERFACE_TYPE_BLE:
            ret = user_ble_data_packge(p_dst, out_buf_size, p_src, byte_len,cmd_type, sequence, func_code);          
            break;
        
        default:break;
    }
    return ret;
}
//end

