

::generate settings
nrfutil settings generate --family NRF52 --application ..\_build\nrf52832_xxaa.hex --application-version 1 --bootloader-version 1 --bl-settings-version 1 setting.hex

::merge
::mergehex.exe -m ..\..\sdk\components\softdevice\s132\hex\s132_nrf52_6.0.0_softdevice.hex ..\..\sdk\examples\dfu\secure_bootloader\pca10040_ble_debug\arm5_no_packs\_build\nrf52832_boot.hex -o sd_boot.hex
mergehex.exe -m ..\..\sdk\components\softdevice\s132\hex\s132_nrf52_6.0.0_softdevice.hex nrf52832_boot_sdk15.hex -o sd_boot.hex
mergehex.exe -m sd_boot.hex ..\_build\nrf52832_xxaa.hex -o sd_boot_app.hex
mergehex.exe -m sd_boot_app.hex setting.hex -o .\output\sd_boot_app_setting.hex

::package
nrfutil pkg generate --application ..\_build\nrf52832_xxaa.hex --application-version 0x01 --hw-version 52 --sd-req 0x00A8 --key-file priv.pem .\output\mark_app_dfu.zip

::delete not used file
del sd_boot.hex
del sd_boot_app.hex
del setting.hex