

::nrfutil pkg generate --application nrf52832_app.hex --application-version 0x01 --hw-version 52 --sd-req 0x009d --key-file priv.pem nrf52832_ota_app.zip

::mergehex.exe -m nrf52_sdk.hex nrf52_boot.hex -o sd_boot.hex
::mergehex.exe -m sd_boot.hex nrf52_app.hex -o sd_boot_app.hex
::mergehex.exe -m sd_boot_app.hex nrf52_setting.hex -o sd_boot_app_setting.hex

::generate the settings file
::nrfutil settings generate --family NRF52 --application nrf52_app.hex --application-version 1 --bootloader-version 1 --bl-settings-version 1 nrf52_setting.hex


nrfjprog.exe --family NRF52 --eraseall
nrfjprog.exe --family NRF52 --program .\output\sd_boot_app_setting.hex --verify --reset
